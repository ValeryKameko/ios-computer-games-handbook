//
//  ReplaceGameDetailsViewSegue.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/29/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation
import UIKit

class ReplaceGameDetailsViewSegue: UIStoryboardSegue {
    override func perform() {
        let src = self.source
        let dst = self.destination
        
        if let navigationController = src.navigationController {
            navigationController.popViewController(animated: false)
            navigationController.pushViewController(dst, animated: true)
        }
    }
}
