//
//  GamesTableViewCell.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/15/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation
import UIKit
import Cosmos
import WSTagsField

class GamesTableViewCell: UITableViewCell {
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var addedByLabel: UILabel!
    @IBOutlet weak var genresLabel: UILabel!
    
    @IBOutlet weak var pictureView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var addedByView: UILabel!
    @IBOutlet weak var descriptionView: UILabel!
    @IBOutlet weak var genresTagsView: WSTagsField!
    
    func initializeWith(game: Game) throws {
        let mediaStorage = try StorageFactory.shared.getMediaStorage()

        titleLabel.text = game.name
        ratingView.rating = game.rating
        addedByView.text = String(game.addedCount)
        descriptionView.text = game.description
        
        genresTagsView.removeTags()
        genresTagsView.font = .systemFont(ofSize: 12.0)
        genresTagsView.layoutMargins = UIEdgeInsets(top: 3, left: 5, bottom: 3, right: 5)
        genresTagsView.contentInset = UIEdgeInsets(top: 5, left: 7, bottom: 5, right: 7)
        genresTagsView.addTags(game.genres.map { $0.name })
        
        if let pictureURL = game.imageURL {
            mediaStorage.fetchImage(url: pictureURL)
                .done(on: DispatchQueue.main) { picture in
                    if let picture = picture {
                        self.pictureView.image = picture
                    } else {
                        self.pictureView.image = UIImage(named: "image_not_found")
                    }
                }
        } else {
            self.pictureView.image = UIImage(named: "image_not_found")
        }
        
        setupStrings()
        applyCustomFont()
    }
    
    private func setupStrings() {
        ratingLabel.text = "Rating:".localized
        addedByLabel.text = "Added by:".localized
        genresLabel.text = "Genres:".localized
    }
}
