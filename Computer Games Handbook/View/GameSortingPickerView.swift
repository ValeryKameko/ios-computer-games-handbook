//
//  SortingPickerView.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/22/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation
import UIKit

class GameSortingPickerView : UIPickerView, UIPickerViewDelegate, UIPickerViewDataSource {

    public private(set) var sorting: FieldSorting<GameField> = FieldSorting<GameField>(field: .none, sortOrder: .asc)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.delegate = self
        self.dataSource = self
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)

        self.delegate = self
        self.dataSource = self
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch component {
        case 0:
            return GameField.values().count
        case 1:
            return SortOrder.values().count
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch component {
        case 0:
            return GameField.values()[row].name
        case 1:
            return SortOrder.values()[row].name
        default:
            return nil
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch component {
        case 0:
            let newField = GameField.values()[row]
            self.sorting = FieldSorting<GameField>(field: newField, sortOrder: sorting.sortOrder)
        case 1:
            let newSortOrder = SortOrder.values()[row]
            self.sorting = FieldSorting<GameField>(field: sorting.field, sortOrder: newSortOrder)
        default:
            return
        }
    }
    
    func setSorting(sorting: FieldSorting<GameField>) {
        self.sorting = sorting
        
        self.selectRow(GameField.values().firstIndex { $0 === sorting.field }!, inComponent: 0, animated: true)
        self.selectRow(SortOrder.values().firstIndex { $0 === sorting.sortOrder }!, inComponent: 1, animated: true)
    }
}
