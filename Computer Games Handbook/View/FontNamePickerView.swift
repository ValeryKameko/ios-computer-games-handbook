//
//  FontPickerView.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/23/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation
import UIKit

final class FontNamePickerView: UITextField, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    private let pickerView = UIPickerView()
    var availableFontNames: [String] = [] {
        didSet {
            fontName = availableFontNames.first
        }
    }
    var fontName: String! {
        didSet {
            if let fontName = self.fontName {
                text = fontName
                font = UIFont(name: fontName, size: 12.0) ?? UIFont.systemFont(ofSize: 12.0)
            }
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.initialize()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialize()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        availableFontNames.count
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let fontName = availableFontNames[row]
        let font = UIFont(name: fontName, size: 12.0) ?? UIFont.systemFont(ofSize: 12.0)
        let attributes = [NSAttributedString.Key.font: font]
        return NSAttributedString(string: fontName, attributes: attributes)
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        fontName = availableFontNames[row]
    }
    
    private func initialize() {
        availableFontNames = UIFont.familyNames.flatMap { UIFont.fontNames(forFamilyName: $0) }
        
        pickerView.delegate = self
        pickerView.dataSource = self
        
        inputView = pickerView
        inputAccessoryView = buildToolbar()
    }
    
    private func buildToolbar() -> UIToolbar {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(
            title: "Done".localized,
            style: .plain,
            target: self,
            action: #selector(self.doneHandler))
        toolbar.setItems([doneButton], animated: true)
        toolbar.isUserInteractionEnabled = true
        
        return toolbar
    }
    
    @objc private func doneHandler() {
        endEditing(false)
        
        let selectedRow = pickerView.selectedRow(inComponent: 0)
        fontName = availableFontNames[selectedRow]
    }
}
