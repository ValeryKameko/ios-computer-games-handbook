//
//  SelectPickerView.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/23/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation
import UIKit

final class LanguagePickerView: UITextField, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    private let pickerView = UIPickerView()
    var availableLanguages: [Locale] = [] {
        didSet {
            language = availableLanguages.first
        }
    }
    var language: Locale! {
        didSet {
            if let language = language {
                text = language.localizedString(forIdentifier: language.identifier)
            }
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.initialize()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialize()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        availableLanguages.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if let language = availableLanguages[safe: row] {
            return language.localizedString(forIdentifier: language.identifier)
        } else {
            return nil
        }
    }
    
    private func initialize() {
        pickerView.delegate = self
        pickerView.dataSource = self
        
        inputView = pickerView
        inputAccessoryView = buildToolbar()
    }
    
    private func buildToolbar() -> UIToolbar {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(
            title: "Done".localized,
            style: .plain,
            target: self,
            action: #selector(self.doneHandler))
        toolbar.setItems([doneButton], animated: true)
        toolbar.isUserInteractionEnabled = true
        
        return toolbar
    }
    
    @objc private func doneHandler() {
        endEditing(false)
        
        let selectedRow = pickerView.selectedRow(inComponent: 0)
        language = availableLanguages[selectedRow]
    }
}
