//
//  Range.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/23/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

struct ExtendedClosedRange<T: Comparable> {
    let lowerBound: T?
    let upperBound: T?
    
    public static func ~=(pattern: ExtendedClosedRange<T>, value: T) -> Bool {
        switch (pattern.lowerBound, pattern.upperBound) {
        case (nil, nil):
            return true
        case (nil, let upperBound):
            return value <= upperBound!
        case (let lowerBound, nil):
            return lowerBound! <= value
        case (let lowerBound, let upperBound):
            return lowerBound! <= value && value <= upperBound!
        }
    }
}
