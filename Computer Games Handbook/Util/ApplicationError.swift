//
//  Error.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/22/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

enum ApplicationError: LocalizedError {
    case invalidInput(message: String)
    case videoNotFound
    
    var errorDescription: String? {
        switch self {
        case .invalidInput(_):
            return "Invalid input".localized
        case .videoNotFound:
            return "Video not found".localized
        }
    }
    
    var failureReason: String? {
        switch self {
        case .invalidInput(let message):
            return message
        case .videoNotFound:
            return "Corresponding video clip has not been found".localized
        }
    }
}
