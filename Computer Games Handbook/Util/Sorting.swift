//
//  Sorting.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/17/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

class SortOrder {
    static let asc = SortOrder(name: "Ascending")
    static let desc = SortOrder(name: "Descending")
    
    private let plainName: String
    
    var name: String {
        get {
            plainName.localized
        }
    }
    
    private init(name: String) {
        self.plainName = name
    }
    
    static func values() -> [SortOrder] {
        return [.asc, .desc]
    }
}

class FieldSorting<F> {
    let field: F
    let sortOrder: SortOrder
    
    init(field: F, sortOrder: SortOrder) {
        self.field = field
        self.sortOrder = sortOrder
    }
}

class FieldsSorting<F> {
    let fieldSortings: [FieldSorting<F>]
    
    init(fieldSortings: [FieldSorting<F>]? = nil) {
        self.fieldSortings = fieldSortings ?? []
    }
}
