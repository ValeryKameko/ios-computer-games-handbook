//
//  Result.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/22/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

enum Result<T> {
    case error(_ error: Error)
    case success(_ value: T)
}

