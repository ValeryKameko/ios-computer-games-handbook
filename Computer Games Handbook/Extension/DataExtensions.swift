//
//  DataExtensions.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/17/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation
import CommonCrypto

extension Data {
    func sha1() -> Data {
        var digest = [UInt8](repeating: 0, count: Int(CC_SHA1_DIGEST_LENGTH))
        self.withUnsafeBytes { _ = CC_SHA1($0.baseAddress, CC_LONG(self.count), &digest) }
        return Data(digest)
    }
    
    func hexString() -> String {
        return map { String(format: "%02hhx", $0) }.joined()
    }
}
