//
//  Array.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/15/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

extension Collection {
    subscript (safe index: Self.Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
