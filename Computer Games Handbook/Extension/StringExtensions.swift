//
//  StringExtensions.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/17/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

extension String {
    subscript(_ range: NSRange) -> String {
        let start = self.index(self.startIndex, offsetBy: range.lowerBound)
        let end = self.index(self.startIndex, offsetBy: range.upperBound)
        return String(self[start..<end])
    }
    
    subscript(_ range: ClosedRange<Int>) -> String {
        let start = self.index(self.startIndex, offsetBy: range.lowerBound)
        let end = self.index(self.startIndex, offsetBy: range.upperBound)
        return String(self[start..<end])
    }
    
    func startsWith(string: String, ignoreCase: Bool = false) -> Bool {
        var options: String.CompareOptions = [.anchored]
        if ignoreCase {
            options.insert(.caseInsensitive)
        }
        guard let range = self.range(of: string, options: options) else {
            return false
        }
        return range.lowerBound == self.startIndex
    }
    
    func localized(language: Locale) -> String {
        let path = Bundle.main.path(forResource: language.identifier, ofType: "lproj")
        if let bundle = Bundle(path: path!) {
            return NSLocalizedString(self, bundle: bundle, value: "", comment: "")
        } else {
            return self
        }
    }

    var localized: String {
        if let languageIdentifier = UserDefaults.standard.string(forKey: SettingsStorage.languageUserDefaultKey) {
            return self.localized(language: Locale(identifier: languageIdentifier))
        } else {
            return self
        }
    }

}
