//
//  NSRegularExpressionExtensions.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/17/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

extension NSRegularExpression {
    func findAll(in string: String) -> [String] {
        let matches = self.matches(in: string,
                                   range: NSRange(location: 0, length: string.utf16.count))
        return matches.map { match in string[match.range(at: 0)] }
    }
}
