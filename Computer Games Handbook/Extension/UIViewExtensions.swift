//
//  UIViewExtensions.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/23/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func setGlobalFont(font: UIFont) {
        switch self {
        case let button as UIButton:
            button.titleLabel?.font = font
        case let label as UILabel:
            label.font = font
        case let textField as UITextField:
            textField.font = font
        case let textView as UITextView:
            textView.font = font
        default:
            subviews.forEach { $0.setGlobalFont(font: font) }
        }
    }
    
    func applyCustomFont() {
        let fontSizeString = UserDefaults.standard.string(forKey: SettingsStorage.fontSizeUserDefaultKey)
        
        let fontSize = fontSizeString
            .flatMap { Float($0) }
            .map { CGFloat($00) } ?? UIFont.systemFontSize
        
        let fontName = UserDefaults.standard.string(forKey: SettingsStorage.fontNameUserDefaultKey)
    
        let font = fontName.flatMap { UIFont(name: $0, size: fontSize) } ??
            UIFont.systemFont(ofSize: fontSize)
        
        setGlobalFont(font: font)
    }
}
