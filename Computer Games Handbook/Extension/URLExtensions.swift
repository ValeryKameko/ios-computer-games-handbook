//
//  URLExtensions.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/16/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

extension URL {
    func relativePath(from base: URL) -> String? {
        guard self.isFileURL && base.isFileURL else {
            return nil
        }
        
        let destComponents = self.pathComponents
        let baseComponents = base.pathComponents
        
        var commonCount = 0
        while commonCount < destComponents.count &&
            commonCount < baseComponents.count &&
            destComponents[commonCount] == baseComponents[commonCount] {
            commonCount += 1
        }
        
        var relativeComponents = Array(repeating: "..", count: baseComponents.count - commonCount)
        relativeComponents.append(contentsOf: destComponents[commonCount...])
        return relativeComponents.joined(separator: "/")
    }
}
