//
//  PromiseKitUtil.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/27/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation
import PromiseKit

extension CatchMixin {
    func catchOnly<Error>(only: Error, body: @escaping (Error) -> Self.T) -> Promise<Self.T>
        where Error: Equatable
    {
        let (promise, resolver) = Promise<Self.T>.pending()

        pipe {
            switch $0 {
            case .rejected(let error as Error) where only == error:
                resolver.fulfill(body(error))
            case .rejected(let error):
                resolver.reject(error)
            case .fulfilled(let value):
                resolver.fulfill(value)
            }
        }
        
        return promise
    }

    func tryCatch(body: @escaping (Error) throws -> Self.T) -> Promise<Self.T>
    {
        let (promise, resolver) = Promise<Self.T>.pending()

        pipe {
            switch $0 {
            case .rejected(let error):
                do {
                    let result = try body(error)
                    resolver.fulfill(result)
                } catch let error {
                    resolver.reject(error)
                }
            case .fulfilled(let value):
                resolver.fulfill(value)
            }
        }
        
        return promise
    }
}
