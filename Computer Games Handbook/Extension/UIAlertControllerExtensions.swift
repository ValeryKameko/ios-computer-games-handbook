//
//  UIAlertControllerExtensions.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/22/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation
import UIKit

extension UIAlertController {
    static func alert(withError: LocalizedError?) -> UIAlertController {
        //TODO: fix translation
        let title = withError?.localizedDescription ?? "Fatal error".localized
        let message = withError?.failureReason ?? "Something went wrong".localized
        
        let res = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        res.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
        
        return res
    }
    
    static func inputAlert(message: String) -> UIAlertController {
        //TODO: fix translation
        let title = "Invalid input".localized
        let res = UIAlertController(title: title, message: message, preferredStyle: .alert)
        res.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
        
        return res
    }
}
