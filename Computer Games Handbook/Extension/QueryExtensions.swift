//
//  QueryExtensions.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/28/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation
import Firebase

extension Query {
    func whereField(field: String, startsWith prefix: String) -> Query {
        if (prefix.isEmpty) {
            return self
        }
    
        let lowerBound = prefix
        let endChar = Character(UnicodeScalar(prefix.last!.unicodeScalars.first!.value + 1)!)
        var upperBound = prefix.prefix(prefix.count - 1)
        upperBound.append(endChar)
        
        return self
            .whereField(field, isGreaterThanOrEqualTo: lowerBound)
            .whereField(field, isLessThan: upperBound)
    }
}
