//
//  ComparisonResultExtensions.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/18/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

extension ComparisonResult {
    func inverted() -> Self {
        switch self {
        case .orderedDescending:
            return .orderedAscending
        case .orderedAscending:
            return .orderedDescending
        default:
            return self
        }
    }
    
    static func combine(lhs: ComparisonResult, rhs: ComparisonResult) -> ComparisonResult {
        if (lhs != .orderedSame) {
            return lhs
        } else {
            return rhs
        }
    }
}
