//
//  FirebaseServiceFactory.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/27/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

class FirebaseServiceFactory {
    var firebaseService: FirebaseService?
    static var shared = FirebaseServiceFactory()
    
    private init() { }
    
    func getGamesFirebaseService() throws -> GamesFirebaseService {
        return try getFirebaseService().gamesService
    }
    
    func getMediaFirebaseService() throws -> MediaFirebaseService {
        return try getFirebaseService().mediaService
    }
    
    func getTagsFirebaseService() throws -> TagsFirebaseService {
        return try getFirebaseService().tagsService
    }
    
    func getGenresFirebaseService() throws -> GenresFirebaseService {
        return try getFirebaseService().genresService
    }
    
    func getPlatformsFirebaseService() throws -> PlatformsFirebaseService {
        return try getFirebaseService().platformsService
    }
    
    func getFirebaseService() throws -> FirebaseService {
        guard let firebaseService = firebaseService else {
            let firebaseService = try self.firebaseService ?? FirebaseService()
            self.firebaseService = firebaseService
            return firebaseService
        }
        return firebaseService
    }
}

