//
//  ServiceError.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/16/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

enum ServiceError: LocalizedError {
    case fileError(fileName: String)
    case loadingError
    
    var errorDescription: String? {
        switch self {
        case .fileError(_):
            return "File error".localized
        case .loadingError:
            return "Loading error".localized
        }
    }
    
    var failureReason: String? {
        switch self {
        case .fileError(let message):
            return message
        case .loadingError:
            return "Unknown error occurred while loading application".localized
        }
    }

}
