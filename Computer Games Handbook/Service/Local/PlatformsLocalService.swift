//
//  TagsLocalService.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/17/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

class PlatformsLocalService {
    var platforms: Set<PlatformLocalView>
    
    init(gamesService: GamesLocalService) {
        let games = gamesService.getGames().items
        platforms = Set(games.flatMap { $0.platforms })
    }
    
    func getPlatforms(query: PlatformsQueryView? = nil) -> Chunk<PlatformLocalView> {
        let platformsSequence = Array(platforms).lazy
        
        let filteredPlatforms = query.map { platformsSequence.filter($0.match(view:)).lazy } ?? platformsSequence
        let count = query?.count ?? filteredPlatforms.count
        
        let platformsPrefix = filteredPlatforms.prefix(count)
        
        return Chunk<PlatformLocalView>(count: count,
                                   items: Array(platformsPrefix))
    }
}
