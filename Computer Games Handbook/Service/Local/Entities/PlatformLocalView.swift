//
//  PlatformView.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/15/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

final class PlatformLocalView: Codable, Hashable {
    let name: String
    
    init(name: String) {
        self.name = name
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        self.name = try container.decode(String.self)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encode(self.name)
    }
    
    static func toView(_ platform: Platform) -> PlatformLocalView {
        PlatformLocalView(name: platform.name)
    }
    
    static func fromView(_ platformView: PlatformLocalView) -> Platform {
        Platform(name: platformView.name)
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.name)
    }
    
    static func == (lhs: PlatformLocalView, rhs: PlatformLocalView) -> Bool {
        return lhs.name == rhs.name
    }
}
