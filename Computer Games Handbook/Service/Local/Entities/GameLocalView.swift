//
//  File.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/15/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

class GameFieldLocalView {
    let value: String?
    
    static let none = GameFieldLocalView(value: nil, comparator: {
        GameFieldLocalView.compareByValue(lhs: $0, rhs: $1) { _ in "" }
    })
    static let name = GameFieldLocalView(value: "name", comparator: {
        GameFieldLocalView.compareByValue(lhs: $0, rhs: $1) { $0.name }
    })
    static let rating = GameFieldLocalView(value: "rating", comparator: {
        GameFieldLocalView.compareByValue(lhs: $0, rhs: $1) { $0.rating }
    })
    static let added = GameFieldLocalView(value: "added", comparator: {
        GameFieldLocalView.compareByValue(lhs: $0, rhs: $1) { $0.added }
    })
    
    let comparator: GameLocalViewsComparator
    
    private init(value: String?,
                 comparator: @escaping GameLocalViewsComparator) {
        self.value = value
        self.comparator = comparator
    }
    
    func selectValue(view: GameLocalView) -> Any? {
        switch self {
        case _ where self === Self.name:
            return view.name
        case _ where self === Self.rating:
            return view.rating
        case _ where self === Self.added:
            return view.added
        default:
            return nil
        }
    }
    
    static func toView(field: GameField) -> GameFieldLocalView {
        switch field {
        case _ where field === Self.name:
            return Self.name
        case _ where field === Self.rating:
            return Self.rating
        case _ where field === Self.added:
            return Self.added
        default:
            return Self.none
        }
    }
    
    private static func compareByValue<T: Comparable>(lhs: GameLocalView, rhs: GameLocalView,
                                                      valueGetter: @escaping (GameLocalView) -> T) -> ComparisonResult {
        let lhsValue = valueGetter(lhs)
        let rhsValue = valueGetter(rhs)
        
        if (lhsValue < rhsValue) {
            return .orderedAscending
        } else if (lhsValue > rhsValue) {
            return .orderedDescending
        } else {
            return .orderedSame
        }
    }
}

final class GameLocalView: Codable {
    private static let DATE_FORMAT = "yyyy-MM-dd"
    
    let id: String
    let name: String
    let rating: Double
    let released: Date
    let added: Int64
    let platforms: [PlatformLocalView]
    let tags: [TagLocalView]
    let genres: [GenreLocalView]
    let description: String
    let websiteSrc: URL?
    let imageSrc: URL?
    let clipSrc: URL?

    enum CodingKeys: String, CodingKey {
        case id
        case name
        case rating
        case released
        case added
        case platforms
        case tags
        case genres
        case description
        case website
        case image
        case clip
    }
    
    init(id: String,
         name: String,
         rating: Double,
         released: Date,
         added: Int64,
         platforms: [PlatformLocalView],
         tags: [TagLocalView],
         genres: [GenreLocalView],
         description: String,
         websiteSrc: URL?,
         imageSrc: URL?,
         clipSrc: URL?) {
        self.id = id
        self.name = name
        self.rating = rating
        self.released = released
        self.added = added
        self.platforms = platforms
        self.tags = tags
        self.genres = genres
        self.description = description
        self.websiteSrc = websiteSrc
        self.imageSrc = imageSrc
        self.clipSrc = clipSrc
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try container.decode(String.self, forKey: .id)
        self.name = try container.decode(String.self, forKey: .name)
        self.rating = try container.decode(Double.self, forKey: .rating)
        self.released = try Self.decodeReleasedDate(from: container)!
        self.added = try container.decode(Int64.self, forKey: .added)
        
        self.platforms = (try? container.decode([PlatformLocalView].self, forKey: .platforms)) ?? []
        self.tags = (try? container.decode([TagLocalView].self, forKey: .tags)) ?? []
        self.genres = (try? container.decode([GenreLocalView].self, forKey: .genres)) ?? []
        
        self.description = try container.decode(String.self, forKey: .description)
        self.websiteSrc = try? container.decode(URL.self, forKey: .website)
        self.imageSrc = try? container.decode(URL.self, forKey: .image)
        self.clipSrc = try? container.decode(URL.self, forKey: .clip)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(self.id, forKey: .id)
        try container.encode(self.name, forKey: .name)
        try container.encode(self.rating, forKey: .rating)
        try Self.encodeReleasedDate(to: &container, released: self.released)
        try container.encode(self.added, forKey: .added)
        
        try container.encode(self.platforms, forKey: .platforms)
        try container.encode(self.tags, forKey: .tags)
        try container.encode(self.genres, forKey: .genres)
        
        try container.encode(self.description, forKey: .description)
        try container.encode(self.websiteSrc, forKey: .website)
        try container.encode(self.imageSrc, forKey: .image)
        try container.encode(self.clipSrc, forKey: .clip)
    }

    static func toView(_ game: Game) -> GameLocalView {
        GameLocalView(id: game.id,
                 name: game.name,
                 rating: game.rating,
                 released: game.releasedDate,
                 added: game.addedCount,
                 platforms: game.platforms.map(PlatformLocalView.toView),
                 tags: game.tags.map(TagLocalView.toView),
                 genres: game.genres.map(GenreLocalView.toView),
                 description: game.description,
                 websiteSrc: game.websiteURL,
                 imageSrc: game.imageURL,
                 clipSrc: game.clipURL)
    }
    
    static func fromView(_ gameView: GameLocalView) -> Game {
        Game(id: gameView.id,
             name: gameView.name,
             rating: gameView.rating,
             releasedDate: gameView.released,
             addedCount: gameView.added,
             platforms: gameView.platforms.map(PlatformLocalView.fromView),
             tags: gameView.tags.map(TagLocalView.fromView),
             genres: gameView.genres.map(GenreLocalView.fromView),
             description: gameView.description,
             websiteURL: gameView.websiteSrc,
             imageURL: gameView.imageSrc,
             clipURL: gameView.clipSrc)
    }
    
    private static func decodeReleasedDate(from container: KeyedDecodingContainer<CodingKeys>) throws -> Date? {
        let releasedString = try container.decode(String.self, forKey: .released)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Self.DATE_FORMAT
        return dateFormatter.date(from: releasedString)
    }
    
    private static func encodeReleasedDate(to container: inout KeyedEncodingContainer<CodingKeys>, released: Date?) throws {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Self.DATE_FORMAT
        let releasedString = released.map({ dateFormatter.string(from: $0) })
        try container.encode(releasedString, forKey: .released)
    }

}
