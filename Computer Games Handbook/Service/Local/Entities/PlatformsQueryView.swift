//
//  PlatformsQueryView.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/17/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

class PlatformsQueryView {
    let prefix: String
    let count: Int
    let ignoreCase: Bool
    
    init(
        prefix: String,
        count: Int,
        ignoreCase: Bool
    ) {
        self.prefix = prefix
        self.count = count
        self.ignoreCase = ignoreCase
    }
    
    convenience init(query: PlatformQuery) {
        self.init(
            prefix: query.prefix ?? "",
            count: query.count,
            ignoreCase: true
        )
    }
    
    func match(view: PlatformLocalView) -> Bool {
        switch view {
        case let view where !view.name.startsWith(string: prefix, ignoreCase: ignoreCase):
            return false
        default:
            return true
        }
    }
}
