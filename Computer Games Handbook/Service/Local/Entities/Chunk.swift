//
//  Chunk.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/15/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

final class Chunk<T> {
    let count: Int
    let items: [T]
    
    init(count: Int, items: [T]) {
        self.count = count
        self.items = items
    }
}
