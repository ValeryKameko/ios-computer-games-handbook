//
//  GamesSorting.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/17/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

typealias GameLocalViewsComparator = (GameLocalView, GameLocalView) -> ComparisonResult

class GameFieldSortingLocalView: FieldSortingLocalView {
    typealias Element = GameLocalView
    typealias Field = GameFieldLocalView
    
    let field: GameFieldLocalView
    let order: SortOrderLocalView
    
    lazy var comparator: GameLocalViewsComparator = { lhs, rhs in
        let comparisonResult = self.field.comparator(lhs, rhs)
        switch self.order {
        case .asc:
            return comparisonResult
        case .desc:
            return comparisonResult.inverted()
        }
    }
    
    init(field: GameFieldLocalView, order: SortOrderLocalView) {
        self.field = field
        self.order = order
    }
    
    convenience init(fieldSorting: FieldSorting<GameField>) {
        self.init(
            field: GameFieldLocalView.toView(field: fieldSorting.field),
            order: SortOrderLocalView.toView(sortOrder: fieldSorting.sortOrder)!
        )
    }    
}

class GameFieldsSortingView: FieldsSortingLocalView {
    typealias Element = GameLocalView
    typealias ElementFieldSorting = GameFieldSortingLocalView
    
    let fieldSortings: [GameFieldSortingLocalView]
    
    lazy var comparator: GameLocalViewsComparator = { lhs, rhs in
        let comparisonResults = self.fieldSortings.map { $0.comparator(lhs, rhs) }
        let comparisonResult = comparisonResults.reduce(ComparisonResult.orderedSame, ComparisonResult.combine)
        return comparisonResult
    }
    
    init(fieldSortings: [GameFieldSortingLocalView]? = nil) {
        self.fieldSortings = fieldSortings ?? []
    }
    
    convenience init(fieldsSorting: FieldsSorting<GameField>) {
        self.init(fieldSortings: fieldsSorting.fieldSortings.map(GameFieldSortingLocalView.init(fieldSorting:)))
    }
}
