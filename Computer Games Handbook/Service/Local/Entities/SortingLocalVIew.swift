//
//  SortingVIew.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/17/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

enum SortOrderLocalView {
    case asc
    case desc
    
    static func toView(sortOrder: SortOrder) -> SortOrderLocalView? {
        switch sortOrder {
        case _ where sortOrder === SortOrder.asc:
            return Self.asc
        case _ where sortOrder === SortOrder.desc:
            return Self.desc
        default:
            assertionFailure("Cannot convert SortOrder to view")
            return nil
        }
    }
}

protocol FieldSortingLocalView {
    associatedtype Element
    associatedtype Field
    
    typealias ElementComparator = (Element, Element) -> ComparisonResult
    
    var field: Field {get}
    var order: SortOrderLocalView {get}
    
    var comparator: ElementComparator {get}
}


protocol FieldsSortingLocalView {
    associatedtype Element
    associatedtype ElementFieldSorting
    
    typealias ElementComparator = (Element, Element) -> ComparisonResult
    
    var fieldSortings: [ElementFieldSorting] {get}
    
    var comparator: ElementComparator {get}
}
