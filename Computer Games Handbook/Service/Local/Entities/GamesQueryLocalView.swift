//
//  GameQueryView.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/17/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

class GamesQueryLocalView {
    let nameSubstring: String?
    let tags: [TagLocalView]?
    let genres: [GenreLocalView]?
    let platforms: [PlatformLocalView]?
    let addedByRange: ExtendedClosedRange<Int64>?
    let ratingRange: ExtendedClosedRange<Double>?
    let sorting: GameFieldsSortingView
    
    init(
        nameSubstring: String? = nil,
        tags: [TagLocalView]? = nil,
        genres: [GenreLocalView]? = nil,
        platforms: [PlatformLocalView]? = nil,
        addedByRange: ExtendedClosedRange<Int64>? = nil,
        ratingRange: ExtendedClosedRange<Double>? = nil,
        sorting: GameFieldsSortingView? = nil
    ) {
        self.nameSubstring = nameSubstring
        self.tags = tags
        self.genres = genres
        self.platforms = platforms
        self.addedByRange = addedByRange
        self.ratingRange = ratingRange
        self.sorting = sorting ?? GameFieldsSortingView()
    }
    
    convenience init(query: GameQuery) {
        self.init(
            nameSubstring: query.titleSubstring,
            tags: query.tags?.map(TagLocalView.toView(_:)),
            genres: query.genres?.map(GenreLocalView.toView(_:)),
            platforms: query.platforms?.map(PlatformLocalView.toView(_:)),
            addedByRange: query.addedByRange.map { ExtendedClosedRange(lowerBound: $0.lowerBound, upperBound: $0.upperBound) },
            ratingRange: query.ratingRange.map { ExtendedClosedRange(lowerBound: $0.lowerBound, upperBound: $0.upperBound) },
            sorting: query.sortingField.map {
                GameFieldsSortingView(fieldSortings: [GameFieldSortingLocalView(fieldSorting: $0)])
            }
        )
    }
    
    func match(view: GameLocalView) -> Bool {
        switch view {
        case _ where nameSubstring != nil &&
            nameSubstring! != "" &&
            !view.name.localizedCaseInsensitiveContains(nameSubstring!):
            return false
        case _ where addedByRange != nil && !(addedByRange! ~= view.added):
            return false
        case _ where ratingRange != nil && !(ratingRange! ~= view.rating):
            return false
        case _ where tags != nil && !(tags!.allSatisfy(view.tags.contains)):
            return false
        case _ where genres != nil && !(genres!.allSatisfy(view.genres.contains)):
            return false
        case _ where platforms != nil && !(platforms!.allSatisfy(view.platforms.contains)):
            return false
        default:
            return true
        }
    }
}
