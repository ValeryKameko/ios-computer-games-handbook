//
//  GenresLocalService.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/17/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

class GenresLocalService {
    var genres: Set<GenreLocalView>
    
    init(gamesService: GamesLocalService) {
        let games = gamesService.getGames().items
        genres = Set(games.flatMap { $0.genres })
    }
    
    func getGenres(query: GenresQueryLocalView? = nil) -> Chunk<GenreLocalView> {
        let genresSequence = Array(genres).lazy
        
        let filteredGenres = query.map { genresSequence.filter($0.match(view:)).lazy } ?? genresSequence
        let count = query?.count ?? filteredGenres.count
        
        let genresPrefix = filteredGenres.prefix(count)
        
        return Chunk<GenreLocalView>(count: count,
                                items: Array(genresPrefix))
    }
}
