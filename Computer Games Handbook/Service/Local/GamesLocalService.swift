//
//  GamesLocalService.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/15/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

class GamesLocalService {
    private let filePath: URL
    private var games: [GameLocalView] = []
    
    init(filePath: URL) throws {
        self.filePath = filePath
        games = try loadGames(from: filePath)
    }
    
    func getGames(query: GamesQueryLocalView? = nil, range: Range<Int>? = nil) -> Chunk<GameLocalView> {
        let gamesSequence = games.lazy
        
        let filteredGames = query.map {
            gamesSequence.filter($0.match(view:)).lazy
        } ?? gamesSequence
        
        let querySorting = query?.sorting
        let processedGames = querySorting.map { sorting in
            let comparator = sorting.comparator
            return filteredGames.sorted(by: Self.toSortingComparator(comparator: comparator)).lazy
        } ?? filteredGames
        
        let offset = range?.startIndex ?? 0
        let count = range?.count ?? gamesSequence.count
        
        let gamesRange = processedGames
            .dropFirst(offset)
            .prefix(count)
        
        return Chunk<GameLocalView>(count: filteredGames.count,
                               items: Array(gamesRange))
    }
    
    func getGame(id: String) -> GameLocalView? {
        return games.first(where: { $0.id == id })
    }
    
    private static func toSortingComparator<Element>(comparator: @escaping (Element, Element) -> ComparisonResult) -> (Element, Element) -> Bool {
        return { lhs, rhs in
            return comparator(lhs, rhs) == .orderedAscending
        }
    }
    
    private func loadGames(from filePath: URL) throws -> [GameLocalView] {
        let fileName = filePath.lastPathComponent
        guard let jsonData = try? Data(contentsOf: filePath) else {
            throw ServiceError.fileError(fileName: fileName)
        }

        guard let games = try? JSONDecoder().decode([GameLocalView].self, from: jsonData) else {
            throw ServiceError.fileError(fileName: fileName)
        }

        return games
    }
}
