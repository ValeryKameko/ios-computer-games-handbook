//
//  SettingsService.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/20/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

class SettingsService {
    private static let settingsFileName = "settings.json";
    private let filePath: URL
    
    init() throws {
        let dataDirectory = try! FileManager.default.url(
            for: .documentDirectory, in: .userDomainMask,
            appropriateFor: nil, create: false)
        self.filePath = dataDirectory.appendingPathComponent(Self.settingsFileName)
    }
    
    func saveSettings(settings: SettingsView) throws {
        guard let jsonData = try? JSONEncoder().encode(settings) else {
            throw ServiceError.fileError(fileName: Self.settingsFileName)
        }
        
        try jsonData.write(to: self.filePath)
    }
    
    func loadSettings() -> SettingsView? {
        guard
            let jsonData = try? Data(contentsOf: filePath),
            let settings = try? JSONDecoder().decode(SettingsView.self, from: jsonData)
        else {
            return nil
        }

        return settings
    }
}
