//
//  File.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/17/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

class TagsLocalService {
    var tags: Set<TagLocalView>
    
    init(gamesService: GamesLocalService) {
        let games = gamesService.getGames().items
        tags = Set(games.flatMap { $0.tags })
    }
    
    func getTags(query: TagsQueryLocalView? = nil) -> Chunk<TagLocalView> {
        let tagsSequence = Array(tags).lazy
        
        let filteredTags = query.map { tagsSequence.filter($0.match(view:)).lazy } ?? tagsSequence
        let count = query?.count ?? filteredTags.count
        
        let tagsPrefix = filteredTags.prefix(count)
        
        return Chunk<TagLocalView>(count: count,
                              items: Array(tagsPrefix))
    }
}
