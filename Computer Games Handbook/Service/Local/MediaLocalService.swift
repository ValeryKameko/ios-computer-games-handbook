//
//  MediaService.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/17/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

class MediaLocalService {
    private static let imagesDirectory = "image"
    private static let clipsDirectory = "clip"
    private static let minHashLength = 20
    private static let fileNamePrefixUnfoldRange = 0...3
   
    private let imageDirectory: URL
    private var imageURLs: Set<String>
    
    private let clipDirectory: URL
    private var clipURLs: Set<String>
    
    init(directory: URL) throws {
        imageDirectory = directory.appendingPathComponent(Self.imagesDirectory)
        imageURLs = try Self.loadImages(from: imageDirectory)
        
        clipDirectory = directory.appendingPathComponent(Self.clipsDirectory)
        clipURLs = try Self.loadClips(from: clipDirectory)
    }
    
    func getImageURL(_ url: URL) -> URL? {
        let imageURL = encodeImageURL(url)
        if (imageURLs.contains(imageURL.absoluteString)) {
            return imageURL
        } else {
            return nil
        }
    }
    
    func getClipURL(_ url: URL) -> URL? {
        let clipURL = encodeClipURL(url)
        if (clipURLs.contains(clipURL.absoluteString)) {
            return clipURL
        } else {
            return nil
        }
    }
    
    private func encodeImageURL(_ url: URL) -> URL {
        let relativeURL = encodeRelativeURL(url)
        return URL(fileURLWithPath: relativeURL.relativePath, relativeTo: imageDirectory)
    }
    
    private func encodeClipURL(_ url: URL) -> URL {
        let relativeURL = encodeRelativeURL(url)
        return URL(fileURLWithPath: relativeURL.relativePath, relativeTo: clipDirectory)
    }
    
    private func encodeRelativeURL(_ url: URL) -> URL {
        let hash = extractHash(url) ?? {
            let data = Data(url.path.utf8)
            let hash = data.sha1()
            return hash.hexString()
        }()
        return URL(fileURLWithPath: hash[Self.fileNamePrefixUnfoldRange], isDirectory: true)
            .appendingPathComponent(hash)
            .appendingPathExtension(url.pathExtension)
    }
    
    private func extractHash(_ url: URL) -> String? {
        let hashRegex = Self.buildHashRegex()
        return hashRegex.findAll(in: url.path).first
    }
    
    private static func loadImages(from imageDirectory: URL) throws -> Set<String> {
        var imageURLs = Set<String>()
        let resourceKeys: [URLResourceKey] = [.isDirectoryKey]
        do {
            let enumerator = FileManager.default.enumerator(at: imageDirectory,
                                                            includingPropertiesForKeys: resourceKeys)!
            for case let fileURL as URL in enumerator {
                let resourceValues = try fileURL.resourceValues(forKeys: Set(resourceKeys))
                if (!resourceValues.isDirectory!) {
                    imageURLs.insert(fileURL.absoluteString)
                }
            }
            return imageURLs
        } catch {
            throw ServiceError.loadingError
        }
    }
    
    private static func loadClips(from clipDirectory: URL) throws -> Set<String> {
        var clipURLs = Set<String>()
        let resourceKeys: [URLResourceKey] = [.isDirectoryKey]
        do {
            let enumerator = FileManager.default.enumerator(at: clipDirectory,
                                                            includingPropertiesForKeys: resourceKeys)!
            for case let fileURL as URL in enumerator {
                let resourceValues = try fileURL.resourceValues(forKeys: Set(resourceKeys))
                if (!resourceValues.isDirectory!) {
                    clipURLs.insert(fileURL.absoluteString)
                }
            }
            return clipURLs
        } catch {
            throw ServiceError.loadingError
        }
    }

    private static func buildHashRegex() -> NSRegularExpression {
        return try! NSRegularExpression(
            pattern: "[a-f0-9]{\(Self.minHashLength),}",
            options: .caseInsensitive)
    }
}
