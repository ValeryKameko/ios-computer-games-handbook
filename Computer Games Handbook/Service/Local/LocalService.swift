//
//  LocalService.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/15/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

final class LocalService {
    private static let initialDataDirectory = "Data"
    private static let gamesFileName = "games.json"
    private static let mediaDirectoryName = "media"
    
    private let dataDirectory: URL
    private let gamesFile: URL
    private let mediaDirectory: URL
    
    let gamesService: GamesLocalService
    let mediaService: MediaLocalService
    let tagsService: TagsLocalService
    let genresService: GenresLocalService
    let platformsService: PlatformsLocalService
    
    init() throws {
        dataDirectory = try! FileManager.default.url(
            for: .documentDirectory, in: .userDomainMask,
            appropriateFor: nil, create: false)
        gamesFile = dataDirectory.appendingPathComponent(Self.gamesFileName)
        mediaDirectory = dataDirectory.appendingPathComponent(Self.mediaDirectoryName)
        
        if (!Self.isInitialized(gamesFile)) {
            try Self.initializeStorage(dataDirectory)
        }
        
        gamesService = try GamesLocalService(filePath: gamesFile)
        mediaService = try MediaLocalService(directory: mediaDirectory)
        tagsService = TagsLocalService(gamesService: gamesService)
        genresService = GenresLocalService(gamesService: gamesService)
        platformsService = PlatformsLocalService(gamesService: gamesService)
    }
    
    private static func isInitialized(_ gamesFile: URL) -> Bool {
        return FileManager.default.fileExists(atPath: gamesFile.path)
    }
    
    private static func initializeStorage(_ dataDirectory: URL) throws {
        let initialDataDirecroryURL = URL(fileURLWithPath: Self.initialDataDirectory)
        do {
            let initialDirectory = Self.buildBundleURL(for: initialDataDirecroryURL)!
            try copyAsset(from: initialDirectory, to: dataDirectory)
        } catch let error {
            print (error)
            throw ServiceError.fileError(fileName: initialDataDirecroryURL.lastPathComponent)
        }
    }
    
    private static func copyAsset(from: URL, to: URL) throws {
        let fileManager = FileManager.default
        let resourceKeys: [URLResourceKey] = [.isDirectoryKey]
        do {
            let enumerator = FileManager.default.enumerator(at: from,
                                                            includingPropertiesForKeys: resourceKeys)!
            for case let fileURL as URL in enumerator {
                let resourceValues = try fileURL.resourceValues(forKeys: Set(resourceKeys))
                if (!resourceValues.isDirectory!) {
                    let relativeFileString = fileURL.relativePath(from: from)!
                    let destinationFileURL = URL(fileURLWithPath: relativeFileString, relativeTo: to)
                    try fileManager.createDirectory(at: destinationFileURL.deletingLastPathComponent(), withIntermediateDirectories: true)
                    try fileManager.replaceItemAt(destinationFileURL, withItemAt: fileURL)
                }
            }
        } catch let error {
            print(error)
            throw ServiceError.loadingError
        }
    }
    
    private static func buildBundleURL(for url: URL) -> URL? {
        let fileNamePart = url.deletingPathExtension().lastPathComponent
        let fileExtension = url.pathExtension
        let relativeDirectory = url.deletingLastPathComponent().path
    
        guard let bundlePath = Bundle.main.path(forResource: fileNamePart, ofType: fileExtension, inDirectory: relativeDirectory) else {
            return nil
        }
        return URL(fileURLWithPath: bundlePath)
    }
}
