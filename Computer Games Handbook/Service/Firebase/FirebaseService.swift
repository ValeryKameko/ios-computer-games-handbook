//
//  FirebaseService.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/27/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation
import Firebase

final class FirebaseService {
    private let firestore: Firestore
    private let storage: Firebase.Storage
    
    let mediaService: MediaFirebaseService
    let genresService: GenresFirebaseService
    let tagsService: TagsFirebaseService
    let platformsService: PlatformsFirebaseService
    let gamesService: GamesFirebaseService
    
    init() throws {
        storage = Firebase.Storage.storage()
        firestore = Firestore.firestore()
        
        mediaService = try MediaFirebaseService(storageReference: storage.reference())
        genresService = try GenresFirebaseService(firestore: firestore)
        platformsService = try PlatformsFirebaseService(firestore: firestore)
        tagsService = try TagsFirebaseService(firestore: firestore)
        gamesService = try GamesFirebaseService(
            firestore: firestore,
            tagsService: tagsService,
            genresService: genresService,
            platformsService: platformsService
        )
    }
}
