//
//  SortingFirebaseVIew.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/28/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation
import Firebase

enum SortOrderFirebaseView {
    case asc
    case desc
    
    static func toView(sortOrder: SortOrder) -> SortOrderFirebaseView? {
        switch sortOrder {
        case _ where sortOrder === SortOrder.asc:
            return Self.asc
        case _ where sortOrder === SortOrder.desc:
            return Self.desc
        default:
            assertionFailure("Cannot convert SortOrder to view")
            return nil
        }
    }
}

protocol FieldSortingFirebaseView {
    associatedtype Element
    associatedtype Field
    
    var field: Field {get}
    var order: SortOrderFirebaseView {get}
    
    func applyQuery(query: Query) -> Query
}


protocol FieldsSortingFirebaseView {
    associatedtype Element
    associatedtype ElementFieldSorting
    
    var fieldSortings: [ElementFieldSorting] {get}
    
    func applyQuery(query: Query) -> Query
}
