//
//  PlatformsQueryView.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/27/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation
import Firebase

class PlatformsQueryFirebaseView {
    let prefix: String
    let count: Int
    
    init(
        prefix: String,
        count: Int
    ) {
        self.prefix = prefix
        self.count = count
    }
    
    convenience init(query: PlatformQuery) {
        self.init(
            prefix: query.prefix ?? "",
            count: query.count
        )
    }
    
    func applyQuery(query: Query) -> Query {
        return query
            .whereField("name", isEqualTo: prefix)
            .limit(to: count)
    }
}
