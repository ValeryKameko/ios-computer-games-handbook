//
//  TagView.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/27/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

final class TagFirebaseView: Codable, Hashable {
    let name: String
    
    init(name: String) {
        self.name = name
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        self.name = try container.decode(String.self)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encode(self.name)
    }
    
    static func toView(_ tag: Tag) -> TagFirebaseView {
        TagFirebaseView(name: tag.name)
    }
    
    static func fromView(_ tagView: TagFirebaseView) -> Tag {
        Tag(name: tagView.name)
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.name)
    }
    
    static func == (lhs: TagFirebaseView, rhs: TagFirebaseView) -> Bool {
        return lhs.name == rhs.name
    }
}
