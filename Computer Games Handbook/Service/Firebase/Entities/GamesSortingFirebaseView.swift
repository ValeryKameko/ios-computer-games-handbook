//
//  GamesSortingFirebaseView.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/28/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation
import Firebase

class GameFieldSortingFirebaseView: FieldSortingFirebaseView {
    typealias Element = GameFirebaseView
    typealias Field = GameFieldFirebaseView
    
    let field: GameFieldFirebaseView
    let order: SortOrderFirebaseView
    
    init(field: GameFieldFirebaseView, order: SortOrderFirebaseView) {
        self.field = field
        self.order = order
    }
    
    convenience init(fieldSorting: FieldSorting<GameField>) {
        self.init(
            field: GameFieldFirebaseView.toView(field: fieldSorting.field),
            order: SortOrderFirebaseView.toView(sortOrder: fieldSorting.sortOrder)!
        )
    }
    
    func applyQuery(query: Query) -> Query {
        if (field === GameFieldFirebaseView.none) {
            return query
        }
        switch order {
        case .asc:
            return query.order(by: field.value!, descending: false)
        case .desc:
            return query.order(by: field.value!, descending: true)
        }
    }
}

class GameFieldsSortingFirebaseView: FieldsSortingFirebaseView {
    typealias Element = GameFirebaseView
    typealias ElementFieldSorting = GameFieldSortingFirebaseView
    
    let fieldSortings: [GameFieldSortingFirebaseView]
    
    init(fieldSortings: [GameFieldSortingFirebaseView]? = nil) {
        self.fieldSortings = fieldSortings ?? []
    }
    
    convenience init(fieldsSorting: FieldsSorting<GameField>) {
        self.init(fieldSortings: fieldsSorting.fieldSortings.map(GameFieldSortingFirebaseView.init(fieldSorting:)))
    }
    
    func applyQuery(query: Query) -> Query {
        return fieldSortings.reduce(query) { resultQuery, fieldSorting in
            fieldSorting.applyQuery(query: resultQuery)
        }
    }
}
