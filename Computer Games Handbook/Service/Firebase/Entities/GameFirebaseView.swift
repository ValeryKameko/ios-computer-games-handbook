//
//  GameView.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/27/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

class GameFieldFirebaseView {
    let value: String?
    
    static let none = GameFieldFirebaseView(value: nil)
    static let name = GameFieldFirebaseView(value: "name")
    static let rating = GameFieldFirebaseView(value: "rating")
    static let added = GameFieldFirebaseView(value: "added")
    
    
    private init(value: String?) {
        self.value = value
    }
    
    func selectValue(view: GameFirebaseView) -> Any? {
        switch self {
        case _ where self === Self.name:
            return view.name
        case _ where self === Self.rating:
            return view.rating
        case _ where self === Self.added:
            return view.added
        default:
            return nil
        }
    }
    
    static func toView(field: GameField) -> GameFieldFirebaseView {
        switch field {
        case _ where field === Self.name:
            return Self.name
        case _ where field === Self.rating:
            return Self.rating
        case _ where field === Self.added:
            return Self.added
        default:
            return Self.none
        }
    }
}

final class GameFirebaseView {
    private static let DATE_FORMAT = "yyyy-MM-dd"
    
    let id: String
    let name: String
    let rating: Double
    let released: Date
    let added: Int64
    let platforms: [PlatformFirebaseView]
    let tags: [TagFirebaseView]
    let genres: [GenreFirebaseView]
    let description: String
    let websiteSrc: URL?
    let imageSrc: URL?
    let clipSrc: URL?

    enum CodingKeys: String, CodingKey {
        case name
        case rating
        case released
        case added
        case platforms
        case tags
        case genres
        case description
        case website
        case image
        case clip
    }
    
    init(id: String,
         name: String,
         rating: Double,
         released: Date,
         added: Int64,
         platforms: [PlatformFirebaseView],
         tags: [TagFirebaseView],
         genres: [GenreFirebaseView],
         description: String,
         websiteSrc: URL?,
         imageSrc: URL?,
         clipSrc: URL?) {
        self.id = id
        self.name = name
        self.rating = rating
        self.released = released
        self.added = added
        self.platforms = platforms
        self.tags = tags
        self.genres = genres
        self.description = description
        self.websiteSrc = websiteSrc
        self.imageSrc = imageSrc
        self.clipSrc = clipSrc
    }
    
    static func toView(_ game: Game) -> GameFirebaseView {
        GameFirebaseView(id: game.id,
                 name: game.name,
                 rating: game.rating,
                 released: game.releasedDate,
                 added: game.addedCount,
                 platforms: game.platforms.map(PlatformFirebaseView.toView),
                 tags: game.tags.map(TagFirebaseView.toView),
                 genres: game.genres.map(GenreFirebaseView.toView),
                 description: game.description,
                 websiteSrc: game.websiteURL,
                 imageSrc: game.imageURL,
                 clipSrc: game.clipURL)
    }
    
    static func fromView(_ gameView: GameFirebaseView) -> Game {
        Game(id: gameView.id,
             name: gameView.name,
             rating: gameView.rating,
             releasedDate: gameView.released,
             addedCount: gameView.added,
             platforms: gameView.platforms.map(PlatformFirebaseView.fromView),
             tags: gameView.tags.map(TagFirebaseView.fromView),
             genres: gameView.genres.map(GenreFirebaseView.fromView),
             description: gameView.description,
             websiteURL: gameView.websiteSrc,
             imageURL: gameView.imageSrc,
             clipURL: gameView.clipSrc)
    }
    
    static func decodeReleasedDate(from container: KeyedDecodingContainer<CodingKeys>) throws -> Date? {
        let releasedString = try container.decode(String.self, forKey: .released)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Self.DATE_FORMAT
        return dateFormatter.date(from: releasedString)
    }
    
    static func encodeReleasedDate(to container: inout KeyedEncodingContainer<CodingKeys>, released: Date?) throws {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Self.DATE_FORMAT
        let releasedString = released.map({ dateFormatter.string(from: $0) })
        try container.encode(releasedString, forKey: .released)
    }
}
