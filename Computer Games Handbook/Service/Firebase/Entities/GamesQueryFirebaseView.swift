//
//  GamesQueryFirebaseView.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/28/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation
import Firebase

class GamesQueryFirebaseView {
    let nameSubstring: String?
    let tags: [TagFirebaseView]?
    let genres: [GenreFirebaseView]?
    let platforms: [PlatformFirebaseView]?
    let addedByRange: ExtendedClosedRange<Int64>?
    let ratingRange: ExtendedClosedRange<Double>?
    let sorting: GameFieldsSortingFirebaseView
    
    init(
        nameSubstring: String? = nil,
        tags: [TagFirebaseView]? = nil,
        genres: [GenreFirebaseView]? = nil,
        platforms: [PlatformFirebaseView]? = nil,
        addedByRange: ExtendedClosedRange<Int64>? = nil,
        ratingRange: ExtendedClosedRange<Double>? = nil,
        sorting: GameFieldsSortingFirebaseView? = nil
    ) {
        self.nameSubstring = nameSubstring
        self.tags = tags
        self.genres = genres
        self.platforms = platforms
        self.addedByRange = addedByRange
        self.ratingRange = ratingRange
        self.sorting = sorting ?? GameFieldsSortingFirebaseView()
    }
    
    convenience init(query: GameQuery) {
        self.init(
            nameSubstring: query.titleSubstring,
            tags: query.tags?.map(TagFirebaseView.toView(_:)),
            genres: query.genres?.map(GenreFirebaseView.toView(_:)),
            platforms: query.platforms?.map(PlatformFirebaseView.toView(_:)),
            addedByRange: query.addedByRange.map { ExtendedClosedRange(lowerBound: $0.lowerBound, upperBound: $0.upperBound) },
            ratingRange: query.ratingRange.map { ExtendedClosedRange(lowerBound: $0.lowerBound, upperBound: $0.upperBound) },
            sorting: query.sortingField.map {
                GameFieldsSortingFirebaseView(fieldSortings: [GameFieldSortingFirebaseView(fieldSorting: $0)])
            }
        )
    }
    
    func applyQuery(query: Query) -> Query {
        var resultQuery = query
        
        if let nameSubstring = nameSubstring {
            resultQuery = resultQuery.whereField(field: "name", startsWith: nameSubstring)
        }
        
        resultQuery = sorting.applyQuery(query: resultQuery)
        
        return resultQuery
    }
    
    func match(view: GameFirebaseView) -> Bool {
        switch view {
        case _ where nameSubstring != nil &&
            nameSubstring! != "" &&
            !view.name.localizedCaseInsensitiveContains(nameSubstring!):
            return false
        case _ where addedByRange != nil && !(addedByRange! ~= view.added):
            return false
        case _ where ratingRange != nil && !(ratingRange! ~= view.rating):
            return false
        case _ where tags != nil && !(tags!.allSatisfy(view.tags.contains)):
            return false
        case _ where genres != nil && !(genres!.allSatisfy(view.genres.contains)):
            return false
        case _ where platforms != nil && !(platforms!.allSatisfy(view.platforms.contains)):
            return false
        default:
            return true
        }
    }
}
