//
//  GenreView.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/27/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

final class GenreFirebaseView: Hashable {
    let name: String
    
    init(name: String) {
        self.name = name
    }
    
    static func toView(_ genre: Genre) -> GenreFirebaseView {
        GenreFirebaseView(name: genre.name)
    }
    
    static func fromView(_ genreView: GenreFirebaseView) -> Genre {
        Genre(name: genreView.name)
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.name)
    }
    
    static func == (lhs: GenreFirebaseView, rhs: GenreFirebaseView) -> Bool {
        return lhs.name == rhs.name
    }
}
