//
//  Cursor.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/27/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation
import Firebase

struct Cursor<T> {
    let items: [T]
    let nextKey: DocumentSnapshot?
}
