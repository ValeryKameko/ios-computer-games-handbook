//
//  TagsFirebaseService.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/27/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation
import Firebase
import PromiseKit

final class TagsFirebaseService {
    private let tagsCollection: CollectionReference
    private let cache = NSCache<NSString, TagFirebaseView>()
    
    init(firestore: Firestore) throws {
        tagsCollection = firestore.collection("tags")
        cache.countLimit = 30
    }
    
    func getTags(query: TagsQueryFirebaseView?) -> Promise<[TagFirebaseView]> {
        var firebaseQuery: Query = tagsCollection
        if query != nil {
            firebaseQuery = query!.applyQuery(query: firebaseQuery)
        }
        
        return Promise {
            firebaseQuery.getDocuments(completion: $0.resolve)
        }.map { tagsSnapshot in
            tagsSnapshot.documents.compactMap { self.deserializeTag(document: $0) }
        }
    }

    
    func getTag(id: String) -> Promise<TagFirebaseView?> {
        let key = id as NSString
        if let tagView = cache.object(forKey: key) {
            return Promise.value(tagView)
        } else {
            let tagDocument = tagsCollection.document(id)
            return Promise {
                tagDocument.getDocument(completion: $0.resolve)
            }.compactMap { (tagSnapshot: DocumentSnapshot) in
                if let tagView = self.deserializeTag(document: tagSnapshot) {
                    self.cache.setObject(tagView, forKey: key)
                    return tagView
                } else {
                    return nil
                }
            }
        }
    }
    
    func addTag(tag: TagFirebaseView) -> Promise<DocumentReference> {
        let sameTagsQuery = tagsCollection.whereField("name", isEqualTo: tag.name)
        return Promise<QuerySnapshot?> {
            sameTagsQuery.getDocuments(completion: $0.resolve)
        }.then { querySnapshot -> Promise<DocumentReference> in
            let (promise, resolver) = Promise<DocumentReference>.pending()
            
            if let document = querySnapshot?.documents.first(where: { $0.exists }) {
                resolver.fulfill(document.reference)
                return promise
            }
            
            let tagSnapshot = self.serializeTag(tag: tag)
            var reference: DocumentReference? = nil
            reference = self.tagsCollection.addDocument(data: tagSnapshot) { error in
                resolver.resolve(error, reference)
            }
            return promise
        }
    }
    
    private func deserializeTag(document: DocumentSnapshot) -> TagFirebaseView? {
        if (!document.exists) {
            return nil
        }
        return TagFirebaseView(
            name: document.get("name") as! String
        )
    }
    
    private func serializeTag(tag: TagFirebaseView) -> [String : Any] {
        return [
            "name": tag.name
        ]
    }
}

