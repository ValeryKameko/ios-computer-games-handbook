//
//  GenresFirebaseService.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/27/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation
import Firebase
import PromiseKit

final class GenresFirebaseService {
    private let genresCollection: CollectionReference
    private let cache = NSCache<NSString, GenreFirebaseView>()
    
    init(firestore: Firestore) throws {
        genresCollection = firestore.collection("genres")
        cache.countLimit = 30
    }
    
    func getGenres(query: GenresQueryFirebaseView?) -> Promise<[GenreFirebaseView]> {
        var firebaseQuery: Query = genresCollection
        if query != nil {
            firebaseQuery = query!.applyQuery(query: firebaseQuery)
        }
        
        return Promise {
            firebaseQuery.getDocuments(completion: $0.resolve)
        }.map { genresSnapshot in
            genresSnapshot.documents.compactMap { self.deserializeGenre(document: $0) }
        }
    }

    
    func getGenre(id: String) -> Promise<GenreFirebaseView?> {
        let key = id as NSString
        if let genreView = cache.object(forKey: key) {
            return Promise.value(genreView)
        } else {
            let genreDocument = genresCollection.document(id)
            return Promise {
                genreDocument.getDocument(completion: $0.resolve)
            }.compactMap { (genreSnapshot: DocumentSnapshot) in
                if let genreView = self.deserializeGenre(document: genreSnapshot) {
                    self.cache.setObject(genreView, forKey: key)
                    return genreView
                } else {
                    return nil
                }
            }
        }
    }
    
    func addGenre(genre: GenreFirebaseView) -> Promise<DocumentReference> {
        let sameGenresQuery = genresCollection.whereField("name", isEqualTo: genre.name)
        return Promise<QuerySnapshot?> {
            sameGenresQuery.getDocuments(completion: $0.resolve)
        }.then { querySnapshot -> Promise<DocumentReference> in
            let (promise, resolver) = Promise<DocumentReference>.pending()
            
            if let document = querySnapshot?.documents.first(where: { $0.exists }) {
                resolver.fulfill(document.reference)
                return promise
            }
            
            let genreSnapshot = self.serializeGenre(genre: genre)
            var reference: DocumentReference? = nil
            reference = self.genresCollection.addDocument(data: genreSnapshot) { error in
                resolver.resolve(error, reference)
            }
            return promise
        }
    }
    
    private func deserializeGenre(document: DocumentSnapshot) -> GenreFirebaseView? {
        if (!document.exists) {
            return nil
        }
        return GenreFirebaseView(
            name: document.get("name") as! String
        )
    }
    
    private func serializeGenre(genre: GenreFirebaseView) -> [String : Any] {
        return [
            "name": genre.name
        ]
    }
}
