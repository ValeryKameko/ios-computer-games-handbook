//
//  PlatformsFirebaseService.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/27/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation
import Firebase
import PromiseKit

final class PlatformsFirebaseService {
    private let platformsCollection: CollectionReference
    private let cache = NSCache<NSString, PlatformFirebaseView>()
    
    init(firestore: Firestore) throws {
        platformsCollection = firestore.collection("platforms")
        cache.countLimit = 30
    }
    
    func getPlatforms(query: PlatformsQueryFirebaseView?) -> Promise<[PlatformFirebaseView]> {
        var firebaseQuery: Query = platformsCollection
        if query != nil {
            firebaseQuery = query!.applyQuery(query: firebaseQuery)
        }
        
        return Promise {
            firebaseQuery.getDocuments(completion: $0.resolve)
        }.map { platformsSnapshot in
            platformsSnapshot.documents.compactMap { self.deserializePlatform(document: $0) }
        }
    }

    
    func getPlatform(id: String) -> Promise<PlatformFirebaseView?> {
        let key = id as NSString
        if let platformView = cache.object(forKey: key) {
            return Promise.value(platformView)
        } else {
            let platformDocument = platformsCollection.document(id)
            return Promise {
                platformDocument.getDocument(completion: $0.resolve)
            }.compactMap { (platformSnapshot: DocumentSnapshot) in
                if let platformView = self.deserializePlatform(document: platformSnapshot) {
                    self.cache.setObject(platformView, forKey: key)
                    return platformView
                } else {
                    return nil
                }
            }
        }
    }
    
    func addPlatform(platform: PlatformFirebaseView) -> Promise<DocumentReference> {
        let sameplatformsQuery = platformsCollection.whereField("name", isEqualTo: platform.name)
        return Promise<QuerySnapshot?> {
            sameplatformsQuery.getDocuments(completion: $0.resolve)
        }.then { querySnapshot -> Promise<DocumentReference> in
            let (promise, resolver) = Promise<DocumentReference>.pending()
            
            if let document = querySnapshot?.documents.first(where: { $0.exists }) {
                resolver.fulfill(document.reference)
                return promise
            }
            
            let platformSnapshot = self.serializePlatform(platform: platform)
            var reference: DocumentReference? = nil
            reference = self.platformsCollection.addDocument(data: platformSnapshot) { error in
                resolver.resolve(error, reference)
            }
            return promise
        }
    }
    
    private func deserializePlatform(document: DocumentSnapshot) -> PlatformFirebaseView? {
        if (!document.exists) {
            return nil
        }
        return PlatformFirebaseView(
            name: document.get("name") as! String
        )
    }
    
    private func serializePlatform(platform: PlatformFirebaseView) -> [String : Any] {
        return [
            "name": platform.name
        ]
    }
}

