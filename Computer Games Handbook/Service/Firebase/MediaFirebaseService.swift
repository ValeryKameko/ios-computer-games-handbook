//
//  MediaFirebaseService.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/27/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation
import Firebase
import PromiseKit

final class MediaFirebaseService {
    private static let imagesPath = "image"
    private static let clipsPath = "clip"
    private static let minHashLength = 20
    private static let fileNamePrefixUnfoldRange = 0...3
    private static let maxFileSize: Int64 = 16 * 1024 * 1024
    
    private let clipsReference: StorageReference
    private let imagesReference: StorageReference
    private let storageReference: StorageReference
    
    init(storageReference: StorageReference) throws {
        self.storageReference = storageReference
        imagesReference = storageReference.child(Self.imagesPath)
        clipsReference = storageReference.child(Self.clipsPath)
    }
    
    func getImageURL(url: URL) -> Promise<URL?> {
        let imageURL = encodeImageURL(url)
        let imageReference = getReference(imageURL)
        
        return Promise<URL?> {
            imageReference.downloadURL(completion: $0.resolve)
        }.tryCatch { error in
            try self.handleNotFound(error: error, defaultValue: nil)
        }
    }
    
    func getClipURL(url: URL) -> Promise<URL?> {
        let clipURL = encodeClipURL(url)
        let clipReference = getReference(clipURL)
        
        return Promise<URL?> {
            clipReference.downloadURL(completion: $0.resolve)
        }.tryCatch { error in
            try self.handleNotFound(error: error, defaultValue: nil)
        }
    }
    
    func addImage(data: Data, `extension`: String) -> Promise<URL> {
        let hash = data.sha1().hexString()
        
        let imageReference = imagesReference
            .child(hash[Self.fileNamePrefixUnfoldRange])
            .child("\(hash).\(`extension`)")
        
        return Promise {
            imageReference.putData(data, metadata: nil, completion: $0.resolve)
        }.then { _ in
            Promise<URL> {
                imageReference.downloadURL(completion: $0.resolve)
            }
        }
    }
    
    func addClip(data: Data, `extension`: String) -> Promise<URL> {
        let hash = data.sha1().hexString()
        
        let clipReference = clipsReference
            .child(hash[Self.fileNamePrefixUnfoldRange])
            .child("\(hash).\(`extension`)")
        
        return Promise {
            clipReference.putData(data, metadata: nil, completion: $0.resolve)
        }.then { _ in
            Promise<URL> {
                clipReference.downloadURL(completion: $0.resolve)
            }
        }
    }
    
    func removeClip(url: URL) -> Promise<Void> {
        let clipURL = encodeClipURL(url)
        let clipReference = getReference(clipURL)
        
        return Promise {
            clipReference.delete(completion: $0.resolve(_:))
        }.tryCatch { error in
            try self.handleNotFound(error: error, defaultValue: ())
        }
    }
    
    func removeImage(url: URL) -> Promise<Void> {
        let imageURL = encodeImageURL(url)
        let imageReference = getReference(imageURL)
        
        return Promise {
            imageReference.delete(completion: $0.resolve(_:))
        }.tryCatch { error in
            try self.handleNotFound(error: error, defaultValue: ())
        }
    }
    
    func getImageData(url: URL) -> Promise<Data?> {
        let imageURL = encodeImageURL(url)
        let imageReference = getReference(imageURL)
        print("Loading image \(imageReference.fullPath) from \(imageURL) by \(url)")
        return Promise {
            imageReference.getData(
                maxSize: Self.maxFileSize,
                completion: $0.resolve)
        }.tryCatch { error in
            try self.handleNotFound(error: error, defaultValue: nil)
        }
    }
    
    func getClipData(url: URL) -> Promise<Data?> {
        let clipURL = encodeClipURL(url)
        let clipReference = getReference(clipURL)
        
        return Promise {
            clipReference.getData(
                maxSize: Self.maxFileSize,
                completion: $0.resolve)
        }.tryCatch { error in
            try self.handleNotFound(error: error, defaultValue: nil)
        }
    }
    
    private func handleNotFound<T>(error: Error, defaultValue: T) throws -> T {
        guard
            let errorCode = (error as? NSError)?.code,
            let storageError = StorageErrorCode(rawValue: errorCode) else {
                throw error
            }
        switch storageError {
        case .objectNotFound:
            return defaultValue
        default:
            throw error
        }
    }
    
    private func encodeImageURL(_ url: URL) -> URL {
        let relativeURL = encodeRelativeURL(url)
        return URL(string: "\(Self.imagesPath)/\(relativeURL.relativePath)")!
    }
    
    private func encodeClipURL(_ url: URL) -> URL {
        let relativeURL = encodeRelativeURL(url)
        return URL(string: "\(Self.clipsPath)/\(relativeURL.relativePath)")!
    }
    
    private func getReference(_ url: URL) -> StorageReference {
        let pathComponents = url.pathComponents
        return pathComponents
            .reduce(storageReference,
                    { reference, component in return reference.child(component) })
    }
    
    private func encodeRelativeURL(_ url: URL) -> URL {
        let hash = extractHash(url) ?? {
            let data = Data(url.path.utf8)
            let hash = data.sha1()
            return hash.hexString()
        }()
        return URL(fileURLWithPath: hash[Self.fileNamePrefixUnfoldRange],
                   isDirectory: true)
            .appendingPathComponent(hash)
            .appendingPathExtension(url.pathExtension)
    }

    private func extractHash(_ url: URL) -> String? {
        let hashRegex = Self.buildHashRegex()
        return hashRegex.findAll(in: url.path).first
    }
    
    private static func buildHashRegex() -> NSRegularExpression {
        return try! NSRegularExpression(
            pattern: "[a-f0-9]{\(Self.minHashLength),}",
            options: .caseInsensitive)
    }
}
