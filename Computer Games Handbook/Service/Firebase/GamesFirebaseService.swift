//
//  GamesFirebaseService.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/27/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation
import Firebase
import PromiseKit

final class GamesFirebaseService {
    private static let DATE_FORMAT = "yyyy-MM-dd"
    
    private let gamesCollection: CollectionReference
    
    private let tagsService: TagsFirebaseService
    private let genresService: GenresFirebaseService
    private let platformsService: PlatformsFirebaseService
    
    init(firestore: Firestore,
        tagsService: TagsFirebaseService,
        genresService: GenresFirebaseService,
        platformsService: PlatformsFirebaseService) {
        
        gamesCollection = firestore.collection("games")
            
        self.tagsService = tagsService
        self.genresService = genresService
        self.platformsService = platformsService
    }
    
    func getGames(
        query: GamesQueryFirebaseView? = nil,
        currentCursor: DocumentSnapshot? = nil,
        count: Int? = nil
    ) -> Promise<Cursor<GameFirebaseView>> {
        return Promise<QuerySnapshot> {
            var firebaseQuery: Query = gamesCollection
            
            if let query = query {
                firebaseQuery = query.applyQuery(query: firebaseQuery)
            }
            
            if let currentCursor = currentCursor {
                firebaseQuery = firebaseQuery.start(afterDocument: currentCursor)
            }
            
            if let count = count {
                firebaseQuery = firebaseQuery.limit(to: count)
            }
            
            firebaseQuery.getDocuments(completion: $0.resolve)
        }.then { querySnapshot -> Promise<Cursor<GameFirebaseView>> in
            let documents = querySnapshot.documents
            return when(fulfilled: documents.map { self.deserializeGame(document: $0) })
                .map { gameViews -> Cursor<GameFirebaseView> in
                    Cursor(
                        items: gameViews.compactMap { $0 }.filter { gameView in
                            query.map { $0.match(view: gameView) } ?? true
                        },
                        nextKey: documents.last
                    )
                }
        }.then { cursor -> Promise<Cursor<GameFirebaseView>> in
            if (cursor.nextKey == nil || count != nil || cursor.items.count >= count!) {
                return Promise.value(cursor)
            } else {
                return self.getGames(query: query, currentCursor: cursor.nextKey, count: count! - cursor.items.count)
                    .map { nextCursor -> Cursor<GameFirebaseView> in
                        Cursor(items: cursor.items + nextCursor.items, nextKey: nextCursor.nextKey)
                    }
            }
        }
    }
    
    func getGame(id: String) -> Promise<GameFirebaseView?> {
        return Promise { gamesCollection.document(id).getDocument(completion: $0.resolve) }
            .then { document -> Promise<GameFirebaseView?> in self.deserializeGame(document: document) }
            .tryCatch { error in
                try self.handleNotFound(error: error, defaultValue: nil)
            }
    }
    
    func addGame(game: GameFirebaseView) -> Promise<DocumentReference> {
        return serializeGame(game: game)
            .then { gameSnapshot -> Promise<DocumentReference> in
                let (promise, resolver) = Promise<DocumentReference>.pending()

                var reference: DocumentReference? = nil
                reference = self.gamesCollection.addDocument(data: gameSnapshot) { error in
                    resolver.resolve(error, reference)
                }
                return promise
            }
    }
    
    func removeGame(id: String) -> Promise<Void> {
        Promise {
            gamesCollection.document(id).delete(completion: $0.resolve)
        }
    }
    
    private func deserializeGame(document: DocumentSnapshot) -> Promise<GameFirebaseView?> {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Self.DATE_FORMAT
        
        if (!document.exists) {
            return Promise.value(nil)
        }
        
        let platformsPromise = when(
            fulfilled: (document.get("platforms") as! [DocumentReference]).map {
                platformsService.getPlatform(id: $0.documentID)
            }
        ).map { $0.compactMap { $0 } }
        
        let tagsPromise = when(
            fulfilled: (document.get("tags") as! [DocumentReference]).map {
                tagsService.getTag(id: $0.documentID)
            }
        ).map { $0.compactMap { $0 } }
        
        let genresPromise = when(
            fulfilled: (document.get("genres") as! [DocumentReference]).map {
                genresService.getGenre(id: $0.documentID)
            }
        ).map { $0.compactMap { $0 } }
        
        return when(fulfilled: platformsPromise, genresPromise, tagsPromise)
            .map { platforms, genres, tags -> GameFirebaseView? in
                GameFirebaseView(
                    id: document.documentID,
                    name: document.get("name") as! String,
                    rating: document.get("rating") as! Double,
                    released: dateFormatter.date(from: (document.get("released") as! String))!,
                    added: document.get("added") as! Int64,
                    platforms: platforms,
                    tags: tags,
                    genres: genres,
                    description: document.get("description") as! String,
                    websiteSrc: (document.get("website") as? String).flatMap { value in
                        URL(string: value)
                    },
                    imageSrc: (document.get("image") as? String).flatMap { value in
                        URL(string: value)
                    },
                    clipSrc: (document.get("clip") as? String).flatMap { value in
                        URL(string: value)
                    }
                )
            }
    }
    
    private func serializeGame(game: GameFirebaseView) -> Promise<[String : Any]> {
        let platformsPromise = when(fulfilled:
            game.platforms.map { self.platformsService.addPlatform(platform: $0) }
        )
        let genresPromise = when(fulfilled:
            game.genres.map { self.genresService.addGenre(genre: $0) }
        )
        let tagsPromise = when(fulfilled:
            game.tags.map { self.tagsService.addTag(tag: $0) }
        )
        
        return when(fulfilled: platformsPromise, genresPromise, tagsPromise).map
            { platforms, genres, tags -> [String : Any] in
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = Self.DATE_FORMAT
                
                return [
                    "name" : game.name,
                    "rating" : game.rating,
                    "released" : dateFormatter.string(from: game.released),
                    "added" : game.added,
                    "platforms" : game.platforms,
                    "tags" : game.tags,
                    "genres" : game.genres,
                    "description" : game.description,
                    "website" : game.websiteSrc?.absoluteString as Any,
                    "image" : game.imageSrc?.absoluteString as Any,
                    "clip" : game.clipSrc?.absoluteString as Any,
                ]
        }
    }
    
    private func handleNotFound<T>(error: Error, defaultValue: T) throws -> T {
        guard
            let errorCode = (error as? NSError)?.code,
            let storageError = StorageErrorCode(rawValue: errorCode) else {
                throw error
            }
        switch storageError {
        case .objectNotFound:
            return defaultValue
        default:
            throw error
        }
    }
}
