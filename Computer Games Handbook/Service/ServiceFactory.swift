//
//  ServiceFactory.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/20/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

class ServiceFactory {
    static var shared = ServiceFactory()
    var settingsService: SettingsService?
    
    private init() { }

    func getSettingsService() throws -> SettingsService {
        guard let settingsService = settingsService else {
            let settingsService = try self.settingsService ?? SettingsService()
            self.settingsService = settingsService
            return settingsService
        }
        return settingsService
    }
}
