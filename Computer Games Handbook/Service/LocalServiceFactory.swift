//
//  LocalServiceFactory.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/15/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

class LocalServiceFactory {
    var localService: LocalService?
    static var shared = LocalServiceFactory()
    
    private init() { }
    
    func getGamesLocalService() throws -> GamesLocalService {
        return try getLocalService().gamesService
    }
    
    func getMediaLocalService() throws -> MediaLocalService {
        return try getLocalService().mediaService
    }
    
    func getTagsLocalService() throws -> TagsLocalService {
        return try getLocalService().tagsService
    }
    
    func getGenresLocalService() throws -> GenresLocalService {
        return try getLocalService().genresService
    }
    
    func getPlatformsLocalService() throws -> PlatformsLocalService {
        return try getLocalService().platformsService
    }
    
    func getLocalService() throws -> LocalService {
        guard let localService = localService else {
            let localService = try self.localService ?? LocalService()
            self.localService = localService
            return localService
        }
        return localService
    }
}
