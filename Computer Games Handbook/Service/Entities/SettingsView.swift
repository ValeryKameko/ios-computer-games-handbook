//
//  SettignsView.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/20/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation
import UIKit

final class SettingsView: Codable {
    let fontName: String?
    let fontSize: Float?
    let language: String?

    enum CodingKeys: String, CodingKey {
        case fontName
        case fontSize
        case language
    }
    
    init(
        fontName: String?,
        fontSize: Float?,
        language: String?
    ) {
        self.fontName = fontName
        self.fontSize = fontSize
        self.language = language
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.fontName = try container.decodeIfPresent(String.self, forKey: .fontName)
        self.fontSize = try container.decodeIfPresent(Float.self, forKey: .fontSize)
        self.language = try container.decodeIfPresent(String.self, forKey: .language)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(self.fontName, forKey: .fontName)
        try container.encode(self.fontSize, forKey: .fontSize)
        try container.encode(self.language, forKey: .language)
    }
    
    static func fromView(_ view: SettingsView) -> Settings {
        let language = view.language.map { Locale.init(identifier: $0) }
        let fontSize = view.fontSize ?? Float(UIFont.systemFontSize)
        let font = view.fontName.flatMap { UIFont(name: $0, size: CGFloat(fontSize)) }
        
        return Settings(
            language: language ?? Settings.defaultLanguage,
            font: font ?? UIFont.systemFont(ofSize: CGFloat(fontSize))
        )
    }
    
    static func toView(_ settings: Settings) -> SettingsView {
        return SettingsView(
            fontName: settings.font.fontName,
            fontSize: Float(settings.font.pointSize),
            language: settings.language.identifier
        )
    }
}
