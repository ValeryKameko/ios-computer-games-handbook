//
//  TagsLocalService.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/20/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation
import PromiseKit

class TagsStorage {
    private static let maxTagsCountSize = 10
    private let TagsFirebaseService: TagsFirebaseService
    
    init() throws {
        TagsFirebaseService = try FirebaseServiceFactory.shared.getTagsFirebaseService()
    }
    
    func getTags(query: TagQuery?) -> Promise<[Tag]> {
        let fixedQuery = query?.copy(count: min(query!.count, Self.maxTagsCountSize))
        
        return TagsFirebaseService.getTags(
            query: fixedQuery.map(TagsQueryFirebaseView.init(query:))
        ).map { tagViews in
            tagViews.map(TagFirebaseView.fromView(_:))
        }
    }
}
