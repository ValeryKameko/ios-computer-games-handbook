//
//  GenresLocalService.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/20/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation
import PromiseKit

class GenresStorage {
    private static let maxGenresCountSize = 10
    private let genresFirebaseService: GenresFirebaseService
    
    init() throws {
        genresFirebaseService = try FirebaseServiceFactory.shared.getGenresFirebaseService()
    }
    
    func getGenres(query: GenreQuery?) -> Promise<[Genre]> {
        let fixedQuery = query?.copy(count: min(query!.count, Self.maxGenresCountSize))
        
        return genresFirebaseService.getGenres(
            query: fixedQuery.map(GenresQueryFirebaseView.init(query:))
        ).map { genreViews in
            genreViews.map(GenreFirebaseView.fromView(_:))
        }
    }
}
