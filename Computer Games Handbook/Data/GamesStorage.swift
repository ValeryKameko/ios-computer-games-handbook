//
//  GamesLocalStorage.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/18/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation
import PromiseKit
import Firebase

class GamesStorage {
    private static let defaultPerPage = 10;
    private let gamesFirebaseService: GamesFirebaseService
    private let gamesLocalService: GamesLocalService

    init() throws {
        gamesLocalService = try LocalServiceFactory.shared.getGamesLocalService()
        gamesFirebaseService = try FirebaseServiceFactory.shared.getGamesFirebaseService()
    }
    
    func getGamesPage(
        query: GameQuery? = nil,
        cursor: DocumentSnapshot? = nil,
        count: Int? = nil
    ) -> Promise<Cursor<Game>> {
        return gamesFirebaseService.getGames(
            query: query.map { GamesQueryFirebaseView.init(query: $0) },
            currentCursor: cursor,
            count: count
        ).map { cursor in
            Cursor(
                items: cursor.items.map { GameFirebaseView.fromView($0) },
                nextKey: cursor.nextKey
            )
        }
    }
    
    func getGamesPaged(query: GameQuery? = nil) -> PagingSource<Game> {
        return GamesPagingSource(
            gamesStorage: self,
            query: query,
            perPage: Self.defaultPerPage
        )
    }
    
    func getGame(id: String) -> Promise<Game?> {
        return gamesFirebaseService.getGame(id: id)
            .map { gameView in
                gameView.map(GameFirebaseView.fromView(_:))
            }
    }
    
    func addGame(game: Game) -> Promise<String> {
        return gamesFirebaseService.addGame(game: GameFirebaseView.toView(game))
            .map { reference in reference.documentID }
    }
    
    func removeGame(id: String) -> Promise<Void> {
        return gamesFirebaseService.removeGame(id: id)
    }
}
