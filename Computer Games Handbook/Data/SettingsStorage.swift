//
//  SettingsStorage.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/20/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation
import UIKit

class SettingsStorage {
    public static let languageUserDefaultKey = "language_identifier"
    public static let fontNameUserDefaultKey = "font_name"
    public static let fontSizeUserDefaultKey = "font_size"
    
    private let settingsService: SettingsService
    private lazy var settings: Settings = self.loadSettings()
    
    init() throws {
        settingsService = try ServiceFactory.shared.getSettingsService()
    }
    
    func getSettings() -> Settings {
        return settings
    }
    
    func setSettings(settings: Settings) throws {
        self.settings = settings
        try saveSettings(settings: settings)
    }
    
    func getAvailableLanguages() -> [Locale] {
        return [
            Locale(identifier: "ru"),
            Locale(identifier: "en")
        ]
    }
    
    private func saveSettings(settings: Settings) throws {
        cacheSettings(settings: settings)
        
        let settingsView = SettingsView.toView(settings)
        try settingsService.saveSettings(settings: settingsView)
    }
    
    private func loadSettings() -> Settings {
        let settingsView = self.settingsService.loadSettings()
        let settings = convertToSettings(view: settingsView)
        
        cacheSettings(settings: settings)
        return settings
    }
    
    private func convertToSettings(view: SettingsView?) -> Settings {
        let defaultSettings = Settings(
            language: Settings.defaultLanguage,
            font: UIFont.systemFont(ofSize: UIFont.systemFontSize)
        )
        return view.map(SettingsView.fromView(_:)) ?? defaultSettings
    }
    
    private func cacheSettings(settings: Settings) {
        UserDefaults.standard.set(settings.language.identifier, forKey: Self.languageUserDefaultKey)
        UserDefaults.standard.set(settings.font.fontName, forKey: Self.fontNameUserDefaultKey)
        UserDefaults.standard.set(Float(settings.font.pointSize), forKey: Self.fontSizeUserDefaultKey)
    }
}
