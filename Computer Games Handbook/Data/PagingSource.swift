//
//  PagingSource.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/15/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation
import PromiseKit

typealias ResultDelegate<T> = (Result<T>) -> Void

class PagingSource<T> {
    private var firstLoad = true
    private let perPage: Int
    private var isLoading: Bool = false
    internal var isLoaded: Bool = false
    private var loadedItems: [T] = []
    private var resultDelegate: ResultDelegate<Void>? = nil
    
    init(perPage: Int) {
        self.perPage = perPage
    }
    
    func setResultDelegate(resultDelegate: ResultDelegate<Void>?) {
        self.resultDelegate = resultDelegate
    }
    
    func itemAt(_ index: Int) -> T? {
        return loadedItems[safe: index]
    }
    
    func load() -> Bool {
        if willLoad() {
            if !isLoading {
                isLoading = true
                self.loadNextPage(count: perPage)
                    .done(on: DispatchQueue.global()) { items in
                        self.onResult(result: .success(items))
                    }.catch { error in
                        self.onResult(result: .error(error))
                    }
            }
            return true
        } else {
            return false
        }
    }
    
    func willLoad() -> Bool {
        return firstLoad || !isLoaded
    }
    
    func itemsCount() -> Int {
        return loadedItems.count
    }
    
    func loadNextPage(count: Int?) -> Promise<[T]> {
        preconditionFailure("Abstract")
    }
    
    func onResult(result: Result<[T]>) {
        switch result {
        case .error(let error):
            resultDelegate?(.error(error))
        case .success(let items):
            self.firstLoad = false
            loadedItems += items
            resultDelegate?(.success(()))
        }
        isLoading = false
    }
}
