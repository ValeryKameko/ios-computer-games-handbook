//
//  Genre.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/15/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

class Genre {
    let name: String
    
    init(name: String) {
        self.name = name
    }
}
