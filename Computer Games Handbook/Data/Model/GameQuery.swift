//
//  GameQuery.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/20/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Swift
import Foundation

class GameQuery {
    static let defaultQuery = GameQuery(
        titleSubstring: nil,
        tags: [],
        genres: [],
        platforms: [],
        addedByRange: nil,
        ratingRange: ExtendedClosedRange(lowerBound: 0, upperBound: 5),
        sortingField: FieldSorting<GameField>(field: .none, sortOrder: .asc)
    )
    
    let titleSubstring: String?
    let tags: [Tag]?
    let genres: [Genre]?
    let platforms: [Platform]?
    let addedByRange: ExtendedClosedRange<Int64>?
    let ratingRange: ExtendedClosedRange<Double>?
    let sortingField: FieldSorting<GameField>?
    
    init(
        titleSubstring: String?,
        tags: [Tag]?,
        genres: [Genre]?,
        platforms: [Platform]?,
        addedByRange: ExtendedClosedRange<Int64>?,
        ratingRange: ExtendedClosedRange<Double>?,
        sortingField: FieldSorting<GameField>?
    ) {
        self.titleSubstring = titleSubstring
        self.tags = tags
        self.genres = genres
        self.platforms = platforms
        self.addedByRange = addedByRange
        self.ratingRange = ratingRange
        self.sortingField = sortingField
    }
}
