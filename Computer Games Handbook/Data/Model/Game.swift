//
//  Game.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/14/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

final class Game {
    let id: String
    let name: String
    let rating: Double
    let releasedDate: Date
    let addedCount: Int64
    let platforms: [Platform]
    let tags: [Tag]
    let genres: [Genre]
    let description: String
    let websiteURL: URL?
    let imageURL: URL?
    let clipURL: URL?
    
    init(id: String,
         name: String,
         rating: Double,
         releasedDate: Date,
         addedCount: Int64,
         platforms: [Platform],
         tags: [Tag],
         genres: [Genre],
         description: String,
         websiteURL: URL? = nil,
         imageURL: URL? = nil,
         clipURL: URL? = nil) {
        self.id = id
        self.name = name
        self.rating = rating
        self.releasedDate = releasedDate
        self.addedCount = addedCount
        self.platforms = platforms
        self.tags = tags
        self.genres = genres
        self.description = description
        self.websiteURL = websiteURL
        self.imageURL = imageURL
        self.clipURL = clipURL
    }
}
