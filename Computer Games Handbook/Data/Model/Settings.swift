//
//  Settings.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/20/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation
import UIKit

class Settings {
    static let defaultLanguage = Locale(identifier: "en")
    let language: Locale
    let font: UIFont
    
    init(
        language: Locale,
        font: UIFont
    ) {
        self.language = language
        self.font = font
    }
}
