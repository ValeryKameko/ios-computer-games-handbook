//
//  Tag.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/14/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

final class Tag {
    let name: String
    
    init(name: String) {
        self.name = name
    }
}
