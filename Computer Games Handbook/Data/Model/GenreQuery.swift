//
//  GenreQuery.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/20/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

class GenreQuery {
    let prefix: String?
    let count: Int
    
    required init(
        prefix: String? = nil,
        count: Int
    ) {
        self.prefix = prefix
        self.count = count
    }
    
    func copy(
        prefix: String? = nil,
        count: Int? = nil
    ) -> Self {
        return Self(
            prefix: prefix ?? self.prefix,
            count: count ?? self.count
        )
    }
}
