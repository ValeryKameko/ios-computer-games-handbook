//
//  GameField.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/20/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

class GameField {
    static let none = GameField(name: "None")
    static let name = GameField(name: "Name")
    static let rating = GameField(name: "Rating")
    static let added = GameField(name: "Added")
    
    private let plainName: String
    
    var name: String {
        get {
            return plainName.localized
        }
    }
    
    private init(name: String) {
        self.plainName = name
    }
    
    static func values() -> [GameField] {
        return [none, name, rating, added]
    }
}
