//
//  PlatformsLocalService.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/20/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation
import PromiseKit

class PlatformsStorage {
    private static let maxPlatformsCountSize = 10
    private let PlatformsFirebaseService: PlatformsFirebaseService
    
    init() throws {
        PlatformsFirebaseService = try FirebaseServiceFactory.shared.getPlatformsFirebaseService()
    }
    
    func getPlatforms(query: PlatformQuery?) -> Promise<[Platform]> {
        let fixedQuery = query?.copy(count: min(query!.count, Self.maxPlatformsCountSize))
        
        return PlatformsFirebaseService.getPlatforms(
            query: fixedQuery.map(PlatformsQueryFirebaseView.init(query:))
        ).map { platformViews in
            platformViews.map(PlatformFirebaseView.fromView(_:))
        }
    }
}
