//
//  GameFiltersStorage.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/22/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

class GameQueryStorage {
    private var query = GameQuery.defaultQuery
    
    func setQuery(query: GameQuery) {
        self.query = query
    }
    
    func getQuery() -> GameQuery {
        return self.query
    }
}
