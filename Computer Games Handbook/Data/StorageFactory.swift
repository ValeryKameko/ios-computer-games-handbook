//
//  StorageFactory.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/28/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

class StorageFactory {
    var storage: Storage?
    static var shared = StorageFactory()
    
    private init() { }
    
    func getGameQueryStorage() throws -> GameQueryStorage {
        return try getStorage().gameQueryStorage
    }
    
    func getSettingsStorage() throws -> SettingsStorage {
        return try getStorage().settingsStorage
    }
    
    func getPlatformsStorage() throws -> PlatformsStorage {
        return try getStorage().platformsStorage
    }

    func getTagsStorage() throws -> TagsStorage {
        return try getStorage().tagsStorage
    }
    
    func getGenresStorage() throws -> GenresStorage {
        return try getStorage().genresStorage
    }
    
    func getGamesStorage() throws -> GamesStorage {
        return try getStorage().gamesStorage
    }
    
    func getMediaStorage() throws -> MediaStorage {
        return try getStorage().mediaStorage
    }
    
    func getStorage() throws -> Storage {
        guard let storage = storage else {
            let storage = try self.storage ?? Storage()
            self.storage = storage
            return storage
        }
        return storage
    }
}
