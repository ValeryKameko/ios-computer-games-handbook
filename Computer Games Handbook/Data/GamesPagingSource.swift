//
//  GamesLocalPagingSource.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/20/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation
import Firebase
import PromiseKit

class GamesPagingSource : PagingSource<Game> {
    private let gamesStorage: GamesStorage
    private let query: GameQuery?
    private var nextKey: DocumentSnapshot? = nil
    
    init(gamesStorage: GamesStorage, query: GameQuery?, perPage: Int) {
        self.gamesStorage = gamesStorage
        self.query = query
        super.init(perPage: perPage)
    }
    
    override func loadNextPage(count: Int?) -> Promise<[Game]> {
        return gamesStorage.getGamesPage(
            query: query,
            cursor: nextKey,
            count: count
        ).map { cursor in
            if (cursor.nextKey == nil) {
                self.isLoaded = false
            }
            self.nextKey = cursor.nextKey
            return cursor.items
        }
    }
}
