//
//  MediaLocalStorage.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/20/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation
import PromiseKit

class MediaStorage {
    private let mediaLocalService: MediaLocalService
    private let mediaFirebaseService: MediaFirebaseService
    
    init() throws {
        mediaLocalService = try LocalServiceFactory.shared.getMediaLocalService()
        mediaFirebaseService = try FirebaseServiceFactory.shared.getMediaFirebaseService()
    }

    func getImageURL(url: URL) -> Promise<URL?> {
        return mediaFirebaseService.getImageURL(url: url)
    }
    
    func getClipURL(url: URL) -> Promise<URL?> {
        return mediaFirebaseService.getClipURL(url: url)
    }
    
    func fetchImage(url: URL) -> Promise<UIImage?> {
        return getImageData(url: url)
            .map { data in
                data.flatMap { UIImage.init(data: $0) }
            }
    }
    
    func getImageData(url: URL) -> Promise<Data?> {
        return mediaFirebaseService.getImageData(url: url)
    }
    
    func getClipData(url: URL) -> Promise<Data?> {
        return mediaFirebaseService.getClipData(url: url)
    }

    func addImage(data: Data, extension: String) -> Promise<URL> {
        return mediaFirebaseService.addImage(data: data, extension: `extension`)
    }
    
    func addClip(data: Data, extension: String) -> Promise<URL> {
        return mediaFirebaseService.addClip(data: data, extension: `extension`)
    }
    
    func removeImage(url: URL) -> Promise<Void> {
        return mediaFirebaseService.removeImage(url: url)
    }
    
    func removeClip(url: URL) -> Promise<Void> {
        return mediaFirebaseService.removeClip(url: url)
    }
}
