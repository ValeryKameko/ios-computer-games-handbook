//
//  Storage.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/28/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation

class Storage {
    let gameQueryStorage: GameQueryStorage
    let platformsStorage: PlatformsStorage
    let tagsStorage: TagsStorage
    let genresStorage: GenresStorage
    let mediaStorage: MediaStorage
    let gamesStorage: GamesStorage
    let settingsStorage: SettingsStorage
    
    init() throws {
        gameQueryStorage = GameQueryStorage()
        platformsStorage = try PlatformsStorage()
        tagsStorage = try TagsStorage()
        genresStorage = try GenresStorage()
        mediaStorage = try MediaStorage()
        gamesStorage = try GamesStorage()
        settingsStorage = try SettingsStorage()
    }
}
