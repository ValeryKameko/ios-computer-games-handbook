//
//  NewGameViewController.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/28/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation
import UIKit
import Cosmos
import WSTagsField
import AVKit
import PromiseKit

final class NewGameViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    private let imagePicker = UIImagePickerController()
    
    @IBOutlet weak var newGameNavigationItem: UINavigationItem!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var addedByLabel: UILabel!
    @IBOutlet weak var releasedAtLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var genresLabel: UILabel!
    @IBOutlet weak var platformsLabel: UILabel!
    @IBOutlet weak var tagsLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var websiteLabel: UILabel!
    
    @IBOutlet weak var titleView: UITextField!
    @IBOutlet weak var pictureView: UIImageView!
    @IBOutlet weak var addedByView: UITextField!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var releasedAtPicker: UIDatePicker!
    @IBOutlet weak var genresView: WSTagsField!
    @IBOutlet weak var platformsView: WSTagsField!
    @IBOutlet weak var tagsView: WSTagsField!
    @IBOutlet weak var descriptionView: UITextView!
    @IBOutlet weak var websiteView: UITextField!
    
    @IBOutlet weak var spinnerView: UIActivityIndicatorView!
    @IBOutlet weak var createButton: UIButton!
    
    private var picture: UIImage? = nil
    
    private var mediaStorage: MediaStorage!
    private var gamesStorage: GamesStorage!
    private var tagsStorage: TagsStorage!
    private var platformsStorage: PlatformsStorage!
    private var genresStorage: GenresStorage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Self.configureTagsView(tagsView: tagsView)
        Self.configureTagsView(tagsView: platformsView)
        Self.configureTagsView(tagsView: genresView)
        
        do {
            gamesStorage = try StorageFactory.shared.getGamesStorage()
            mediaStorage = try StorageFactory.shared.getMediaStorage()
            tagsStorage = try StorageFactory.shared.getTagsStorage()
            platformsStorage = try StorageFactory.shared.getPlatformsStorage()
            genresStorage = try StorageFactory.shared.getGenresStorage()
        } catch let error {
            handleError(error: error)
        }
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(self.imageSingleTapped(gesture:)))
        singleTap.numberOfTapsRequired = 1
        pictureView.isUserInteractionEnabled = true
        pictureView.addGestureRecognizer(singleTap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.pictureView.image = UIImage(named: "image_not_found")
        super.viewWillAppear(animated)
        setupStrings()
        view.applyCustomFont()
    }
    
    var gameId: String!
    
    @IBAction func onCreateTapped(_ sender: Any) {
        guard let title = titleView.text else {
            showError(message: "Title is empty".localized)
            return
        }
        
        let releasedAt = releasedAtPicker.date
        let tags = tagsView.tags.map({ tag in Tag(name: tag.text) })
        let platforms = platformsView.tags.map({ tag in Platform(name: tag.text) })
        let genres = genresView.tags.map({ tag in Genre(name: tag.text) })
        
        guard let addedBy = self.addedByView.text.flatMap({ Int64($0) }) else {
            self.showError(message: "Added By should be an integer".localized)
            return
        }
        
        var websiteURL: URL? = nil
        if let websiteString = self.websiteView.text, websiteString != "" {
            guard let website = URL(string: websiteString) else {
                self.showError(message: "Website should contain valid URL".localized)
                return
            }
            websiteURL = website
        }
        
        let rating = ratingView.rating
        let description = descriptionView.text ?? ""
        
        spinnerView.startAnimating()
        createButton.isEnabled = false
        
        let imagePromise: Promise<URL?>
        if
            let picture = picture,
            let pictureData = picture.pngData() {
            imagePromise = mediaStorage.addImage(data: pictureData, extension: "png").map { $0 }
        } else {
            imagePromise = Promise.value(nil)
        }
        
        imagePromise.then { imageURL in
            self.gamesStorage.addGame(game: Game(
                id: "",
                name: title,
                rating: rating,
                releasedDate: releasedAt,
                addedCount: addedBy,
                platforms: platforms,
                tags: tags,
                genres: genres,
                description: description,
                websiteURL: websiteURL,
                imageURL: imageURL,
                clipURL: nil
            ))
        }.done(on: DispatchQueue.main) { id in
            print("ok")
            self.gameId = id
            self.performSegue(withIdentifier: "replaceGameDetails", sender: nil)
        }.catch(on: DispatchQueue.main) { error in
            self.handleError(error: error)
        }.finally(on: DispatchQueue.main) {
            self.spinnerView.stopAnimating()
            self.createButton.isEnabled = false
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "replaceGameDetails" {
            let gameDetailsViewController = segue.destination as! GameDetailsViewController
            gameDetailsViewController.gameId = gameId
        }
    }

    @objc private func imageSingleTapped(gesture: UITapGestureRecognizer?) -> Void {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) {
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[.originalImage] as? UIImage {
            self.picture = image
            self.pictureView.contentMode = .scaleAspectFill
            self.pictureView.image = picture?.stretchableImage(withLeftCapWidth: 10, topCapHeight: 10)
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }

    private static func formatReleasedDate(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        return dateFormatter.string(from: date)
    }
    
    private static func configureTagsView(tagsView: WSTagsField) {
        tagsView.font = .systemFont(ofSize: 12.0)
        tagsView.layoutMargins = UIEdgeInsets(top: 3, left: 5, bottom: 3, right: 5)
        tagsView.contentInset = UIEdgeInsets(top: 5, left: 7, bottom: 5, right: 7)
    }
    
    private func setupStrings() {
        if let languageIdentifier = UserDefaults.standard.string(forKey: SettingsStorage.languageUserDefaultKey) {
            releasedAtPicker.locale = Locale(identifier: languageIdentifier)
        }
        websiteLabel.text = "Website:".localized
        websiteView.placeholder = "Enter website".localized
        releasedAtLabel.text = "Released at:".localized
        newGameNavigationItem.title = "New game".localized
        titleLabel.text = "Title:".localized
        tagsLabel.text = "Tags:".localized
        platformsLabel.text = "Platforms:".localized
        genresLabel.text = "Genres:".localized
        addedByLabel.text = "Added by:".localized
        addedByView.placeholder = "Enter Added by".localized
        ratingLabel.text = "Rating:".localized
        descriptionLabel.text = "Description:".localized
        createButton.setTitle("Create".localized, for: .normal)
    }

    private func showError(message: String) {
        let alert = UIAlertController.inputAlert(message: message)
        self.present(alert, animated: true)
    }
    
    private func handleError(error: Error) {
        print(error)
        let alert = UIAlertController.alert(withError: error as? LocalizedError)
        self.present(alert, animated: true)
    }
}
