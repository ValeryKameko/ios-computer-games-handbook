//
//  GameFiltersViewController.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/22/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation
import UIKit
import Cosmos
import WSTagsField

final class GameFiltersViewController: UIViewController {
    @IBOutlet weak var filtersNavigationItem: UINavigationItem!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tagsLabel: UILabel!
    @IBOutlet weak var platformsLabel: UILabel!
    @IBOutlet weak var genresLabel: UILabel!
    @IBOutlet weak var addedByLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var sortingLabel: UILabel!
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var toLabel: UILabel!
    @IBOutlet weak var applyButton: UIButton!
    @IBOutlet weak var clearButton: UIButton!
    
    @IBOutlet weak var titleView: UITextField!
    @IBOutlet weak var tagsView: WSTagsField!
    @IBOutlet weak var platformsView: WSTagsField!
    @IBOutlet weak var genresView: WSTagsField!
    @IBOutlet weak var addedByMinView: UITextField!
    @IBOutlet weak var addedByMaxView: UITextField!
    @IBOutlet weak var ratingMinView: CosmosView!
    @IBOutlet weak var ratingMaxView: CosmosView!
    @IBOutlet weak var sortingPickerView: GameSortingPickerView!
    
    private var gameQueryStorage: GameQueryStorage!
    
    override func viewWillAppear(_ animated: Bool) {
        setGameQuery(query: gameQueryStorage.getQuery())
        super.viewWillAppear(animated)
        setupStrings()
        view.applyCustomFont()
    }
    
    override func viewDidLoad() {
        gameQueryStorage = try! StorageFactory.shared.getGameQueryStorage()
        
        Self.configureTagsView(tagsView: tagsView)
        Self.configureTagsView(tagsView: platformsView)
        Self.configureTagsView(tagsView: genresView)

        setGameQuery(query: gameQueryStorage.getQuery())
        
        ratingMinView.didFinishTouchingCosmos = { rating in
            if (rating > self.ratingMaxView.rating) {
                self.ratingMinView.rating = self.ratingMaxView.rating
            }
        }
        
        ratingMaxView.didFinishTouchingCosmos = { rating in
            if (rating < self.ratingMinView.rating) {
                self.ratingMaxView.rating = self.ratingMinView.rating
            }
        }
    }
    
    @IBAction func onApplyTapped(_ sender: UIButton) {
        let titleSubstring = titleView.text
        let tags = tagsView.tags.map { Tag(name: $0.text) }
        let platforms = platformsView.tags.map { Platform(name: $0.text) }
        let genres = genresView.tags.map { Genre(name: $0.text) }
        
        let addedByMinText = addedByMinView.text ?? ""
        let addedByMin = Int64(addedByMinText)
        if addedByMin == nil && addedByMinText != "" {
            showError(error: ApplicationError.invalidInput(
                message: "Added by Min field should contain integer".localized
            ))
            return
        }
        
        let addedByMaxText = addedByMaxView.text ?? ""
        let addedByMax = Int64(addedByMaxText)
        if addedByMax == nil && addedByMaxText != "" {
            showError(error: ApplicationError.invalidInput(
                message: "Added by Max field should contain integer".localized
            ))
            return
        }
        
        if addedByMin != nil && addedByMax != nil && addedByMin! > addedByMax! {
            showError(error: ApplicationError.invalidInput(
                message: "Added by Min should not be greater than Added by Max".localized
            ))
            return
        }
        
        let ratingMin = ratingMinView.rating
        let ratingMax = ratingMaxView.rating
        
        let sorting = sortingPickerView.sorting
        
        gameQueryStorage.setQuery(query: GameQuery(
            titleSubstring: titleSubstring,
            tags: tags,
            genres: genres,
            platforms: platforms,
            addedByRange: ExtendedClosedRange(lowerBound: addedByMin, upperBound: addedByMax),
            ratingRange: ExtendedClosedRange(lowerBound: ratingMin, upperBound: ratingMax),
            sortingField: sorting
        ))
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClearTapped(_ sender: UIButton) {
        setGameQuery(query: GameQuery.defaultQuery)
        gameQueryStorage.setQuery(query: GameQuery.defaultQuery)
    }
    
    private func setGameQuery(query: GameQuery) {
        titleView.text = query.titleSubstring

        tagsView.removeTags()
        tagsView.addTags(query.tags?.map { $0.name } ?? [])

        platformsView.removeTags()
        platformsView.addTags(query.platforms?.map { $0.name } ?? [])

        genresView.removeTags()
        genresView.addTags(query.genres?.map { $0.name } ?? [])

        addedByMinView.text = query.addedByRange?.lowerBound
            .map { String($0) } ?? ""
        addedByMaxView.text = query.addedByRange?.upperBound
            .map { String($0) } ?? ""

        ratingMinView.rating = query.ratingRange?.lowerBound ?? 0
        ratingMaxView.rating = query.ratingRange?.upperBound ?? 5

        sortingPickerView.setSorting(sorting: query.sortingField ??
            FieldSorting<GameField>(field: .none, sortOrder: .asc))
    }
    
    private func showError(error: LocalizedError) {
        let alert = UIAlertController.alert(withError: error)
        self.present(alert, animated: true)
    }
    
    private static func configureTagsView(tagsView: WSTagsField) {
        tagsView.font = .systemFont(ofSize: 12.0)
        tagsView.layoutMargins = UIEdgeInsets(top: 3, left: 5, bottom: 3, right: 5)
        tagsView.contentInset = UIEdgeInsets(top: 5, left: 7, bottom: 5, right: 7)
    }
    
    private func setupStrings() {
        filtersNavigationItem.title = "Filters".localized
        titleLabel.text = "Title:".localized
        tagsLabel.text = "Tags:".localized
        platformsLabel.text = "Platforms".localized
        genresLabel.text = "Genres:".localized
        addedByLabel.text = "Added by:".localized
        addedByMinView.placeholder = "Min".localized
        addedByMaxView.placeholder = "Max".localized
        ratingLabel.text = "Rating:".localized
        fromLabel.text = "From".localized
        toLabel.text = "To".localized
        sortingLabel.text = "Sorting:".localized
        applyButton.setTitle("Apply".localized, for: .normal)
        clearButton.setTitle("Clear".localized, for: .normal)
    }
}
