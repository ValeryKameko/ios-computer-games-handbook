//
//  GamesTableViewController.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/14/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation
import UIKit

final class GamesTableViewController: UIViewController, UITableViewDataSource,
    UITableViewDelegate {
    @IBOutlet weak var gamesNavigationItem: UINavigationItem!
    @IBOutlet weak var settingsNavigationItem: UIBarButtonItem!
    @IBOutlet weak var filtersNavigationItem: UIBarButtonItem!
    
    @IBOutlet var tableView: UITableView!

    private var gamesStorage: GamesStorage?
    private var gamesPagingSource: PagingSource<Game>?
    
    override func viewWillAppear(_ animated: Bool) {
        do {
            tableView.dataSource = self
            tableView.delegate = self
            
            gamesStorage = try gamesStorage ?? StorageFactory.shared.getGamesStorage()
            
            let query = try StorageFactory.shared.getGameQueryStorage().getQuery()
            gamesPagingSource = gamesStorage?.getGamesPaged(query: query)
            gamesPagingSource?.setResultDelegate(resultDelegate: self.onLoadResult(result:))
            
            gamesPagingSource?.load()
        } catch let error {
            print(error)
            Thread.callStackSymbols.forEach { print($0) }
            let alert = UIAlertController.alert(withError: error as? LocalizedError)
            self.present(alert, animated: true)
        }
        super.viewWillAppear(animated)
        setupStrings()
        view.applyCustomFont()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if gamesPagingSource?.willLoad() ?? false {
            return 2
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return gamesPagingSource?.itemsCount() ?? 0
        } else if section == 1 {
            return 1
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let tableCell = tableView.dequeueReusableCell(withIdentifier: "gameTableCellView")
            let cell = tableCell as! GamesTableViewCell
            
            do {
                let game: Game = gamesPagingSource!.itemAt(indexPath.row)!
                try cell.initializeWith(game: game)
            } catch let error {
                let alert = UIAlertController.alert(withError: error as? LocalizedError)
                self.present(alert, animated: true)
            }
            return cell
        } else {
            let tableCell = tableView.dequeueReusableCell(withIdentifier: "spinnerTableCellView")
            let cell = tableCell as! SpinnerTableViewCell
            cell.startSpinning()
            
            return cell
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if offsetY > contentHeight - scrollView.frame.size.height {
            gamesPagingSource?.load()
        }
    }
    
    private var selectedGameId: String?
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            selectedGameId = self.gamesPagingSource?.itemAt(indexPath.row)?.id
            performSegue(withIdentifier: "showGameDetail", sender: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showGameDetail" {
            let gameDetailsViewController = segue.destination as! GameDetailsViewController
            gameDetailsViewController.gameId = selectedGameId
        }
    }

    private func onLoadResult(result: Result<Void>) {
        DispatchQueue.main.async {
            switch result {
            case .success(_):
                self.tableView.reloadData()
            case .error(let error):
                print(error)
                Thread.callStackSymbols.forEach { print($0) }
                let alert = UIAlertController.alert(withError: error as? LocalizedError)
                self.present(alert, animated: true)
            }
        }
    }
    
    private func setupStrings() {
        gamesNavigationItem.title = "Games".localized
        settingsNavigationItem.title = "Settings".localized
        filtersNavigationItem.title = "Filters".localized
    }
}
