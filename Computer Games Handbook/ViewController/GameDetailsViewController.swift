//
//  GameDetailsViewController.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/22/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation
import UIKit
import Cosmos
import WSTagsField
import AVKit

final class GameDetailsViewController: UIViewController {
    @IBOutlet weak var addedByLabel: UILabel!
    @IBOutlet weak var releasedAtLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var genresLabel: UILabel!
    @IBOutlet weak var platformsLabel: UILabel!
    @IBOutlet weak var tagsLabel: UILabel!
    @IBOutlet weak var websiteBarButtonItem: UIBarButtonItem!
    
    @IBOutlet weak var spinnerView: UIActivityIndicatorView!
    
    @IBOutlet weak var titleView: UILabel!
    @IBOutlet weak var pictureView: UIImageView!
    @IBOutlet weak var addedByView: UILabel!
    @IBOutlet weak var releasedAtView: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var genresView: WSTagsField!
    @IBOutlet weak var platformsView: WSTagsField!
    @IBOutlet weak var tagsView: WSTagsField!
    @IBOutlet weak var descriptionView: UITextView!

    var gameId: String!
    
    private var game: Game?
    private var mediaStorage: MediaStorage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        spinnerView.startAnimating()
        
        do {
            let gamesStorage = try StorageFactory.shared.getGamesStorage()
            mediaStorage = try StorageFactory.shared.getMediaStorage()
            
            try gamesStorage.getGame(id: gameId)
                .done(on: DispatchQueue.main) { game in
                    self.updateGame(game: game!)
                }
        } catch let error {
            handleError(error: error)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupStrings()
        view.applyCustomFont()
    }
    
    private func updateGame(game: Game) {
        self.game = game
        
        titleView.text = game.name
        ratingView.rating = game.rating
        addedByView.text = String(game.addedCount)
        releasedAtView.text = Self.formatReleasedDate(date: game.releasedDate)
        descriptionView.text = game.description
        
        genresView.removeTags()
        Self.configureTagsView(tagsView: genresView)
        genresView.addTags(game.genres.map { $0.name })
        
        tagsView.removeTags()
        Self.configureTagsView(tagsView: tagsView)
        tagsView.addTags(game.tags.map { $0.name })
        
        platformsView.removeTags()
        Self.configureTagsView(tagsView: platformsView)
        platformsView.addTags(game.platforms.map { $0.name })
    
        if let pictureURL = game.imageURL {
            mediaStorage.fetchImage(url: pictureURL)
                .done(on: DispatchQueue.main) { picture in
                    if let picture = picture {
                        self.pictureView.image = picture
                    } else {
                        self.pictureView.image = UIImage(named: "image_not_found")
                    }
                }.ensure {
                    self.spinnerView.stopAnimating()
                }
        } else {
            self.spinnerView.stopAnimating()
            self.pictureView.image = UIImage(named: "image_not_found")
        }

        
        setupStrings()
        view.applyCustomFont()
    }

    @IBAction func onWebsiteAction(_ sender: Any) {
        if let websiteURL = self.game?.websiteURL {
            UIApplication.shared.open(websiteURL)
        }
    }
    
    private static func formatReleasedDate(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        return dateFormatter.string(from: date)
    }
    
    private static func configureTagsView(tagsView: WSTagsField) {
        tagsView.font = .systemFont(ofSize: 12.0)
        tagsView.layoutMargins = UIEdgeInsets(top: 3, left: 5, bottom: 3, right: 5)
        tagsView.contentInset = UIEdgeInsets(top: 5, left: 7, bottom: 5, right: 7)
    }
    
    private func setupStrings() {
        addedByLabel.text = "Added by:".localized
        releasedAtLabel.text = "Released at:".localized
        ratingLabel.text = "Rating:".localized
        genresLabel.text = "Genres:".localized
        platformsLabel.text = "Platforms:".localized
        tagsLabel.text = "Tags:".localized
        websiteBarButtonItem.title = "Website".localized
    }
    
    private func handleError(error: Error) {
        print(error)
        let alert = UIAlertController.alert(withError: error as? LocalizedError)
        self.present(alert, animated: true)
    }
}
