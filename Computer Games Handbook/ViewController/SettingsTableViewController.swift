//
//  SettingsViewController.swift
//  Computer Games Handbook
//
//  Created by Valery Kameko on 9/23/20.
//  Copyright © 2020 ValeryKameko. All rights reserved.
//

import Foundation
import UIKit

final class SettingsTableViewController : UITableViewController  {
    @IBOutlet weak var settingsNavigationItem: UINavigationItem!
    @IBOutlet weak var languageTableViewSection: UITableViewCell!
    @IBOutlet weak var fontTableViewSection: UITableViewCell!
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var fontNameLabel: UILabel!
    @IBOutlet weak var fontSizeLabel: UILabel!
    @IBOutlet weak var applyButton: UIButton!
    @IBOutlet weak var resetButton: UIButton!
    
    @IBOutlet weak var languagePickerView: LanguagePickerView!
    @IBOutlet weak var fontNamePickerView: FontNamePickerView!
    @IBOutlet weak var fontSizeSliderView: UISlider!
    @IBOutlet weak var fontSizeLabelView: UILabel!
    
    private var settingsStorage: SettingsStorage!
    
    override func viewWillAppear(_ animated: Bool) {
        do {
            settingsStorage = try settingsStorage ?? StorageFactory.shared.getSettingsStorage()
            
            languagePickerView.availableLanguages = settingsStorage.getAvailableLanguages()
            
            let settings = settingsStorage.getSettings()
            setSettings(settings: settings)
        } catch let error {
            print(error)
            Thread.callStackSymbols.forEach { print($0) }
            let alert = UIAlertController.alert(withError: error as? LocalizedError)
            self.present(alert, animated: true)
        }
        super.viewWillAppear(animated)
        setupStrings()
        view.applyCustomFont()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        view.applyCustomFont()
        super.viewDidAppear(animated)
    }
    
    @IBAction func onApplyTapped(_ sender: Any) {
        let language = languagePickerView.language!
        let fontName = fontNamePickerView.fontName!
        let fontSize = CGFloat(fontSizeSliderView.value)
        let font = UIFont(name: fontName, size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
        
        let settings = Settings(language: language, font: font)
        do {
            try settingsStorage.setSettings(settings: settings)
            _ = navigationController?.popViewController(animated: true)
        } catch let error {
            print(error)
            Thread.callStackSymbols.forEach { print($0) }
            let alert = UIAlertController.alert(withError: error as? LocalizedError)
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func onResetTapped(_ sender: Any) {
        setSettings(settings: settingsStorage.getSettings())
    }
    
    @IBAction func onSizeSliderValueChanged(_ sender: Any) {
        let fontSize = fontSizeSliderView.value
        fontSizeLabelView.text = String(format: "%.2f", fontSize)
    }
    
    private func setSettings(settings: Settings) {
        languagePickerView.language = settings.language
        fontNamePickerView.fontName = settings.font.fontName
        fontSizeSliderView.value = Float(settings.font.pointSize)
        fontSizeLabelView.text = String(format: "%.2f", fontSizeSliderView.value)
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Language".localized
        case 1:
            return "Font".localized
        default:
            return nil
        }
    }
    
    private func setupStrings() {
        navigationItem.title = "Settings".localized
        languageLabel.text = "Language".localized
        fontNameLabel.text = "Name".localized
        fontSizeLabel.text = "Size".localized
        applyButton.setTitle("Apply".localized, for: .normal)
        resetButton.setTitle("Reset".localized, for: .normal)
    }
}
